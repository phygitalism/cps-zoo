
# Basics
Папка содержит три файла
- back - модуль для сервера
- front-storm - описание ноды для фронта storm
- config - конфиг модуля.

## Config

Содержит информацию о типе модуля, портах и экстра параметры

```javascript
export default {
    type_id: 'pinger',
    extra:{ //дополнительные параметры, эксклюзивные для данного типа
      periods:[3000,500]
    },
    ports: {
      in: [
        {
          type: 'Number',
          default:12,
          id: 'pIn',
          name: 'in',
          desc: 'some desc'
        }
      ],
      out: [
        {
            type: 'Number',
            id: 'pOut',
            name: 'out',
            desc: 'Some desc'
        }
  ]
    }
}
```
