"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PortTypedFactory = exports.TypedPortLabel = exports.TypedPortWidget = exports.PortTypedModel = exports.defaultTypesValues = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _stormReactDiagrams = require("storm-react-diagrams");

var SRD = _interopRequireWildcard(_stormReactDiagrams);

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _react = require("react");

var React = _interopRequireWildcard(_react);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var typesSigns = {
  'boolean': 'B',
  'double': 'D',
  'int': 'I',
  'string': 'S'
};

var defaultTypesValues = exports.defaultTypesValues = {
  'boolean': false,
  'double': 0,
  'int': 0,
  'string': ''
};

var PortTypedModel = exports.PortTypedModel = function (_SRD$DefaultPortModel) {
  _inherits(PortTypedModel, _SRD$DefaultPortModel);

  function PortTypedModel(type, isInput, name) {
    var label = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'port';
    var id = arguments[4];

    _classCallCheck(this, PortTypedModel);

    var _this = _possibleConstructorReturn(this, (PortTypedModel.__proto__ || Object.getPrototypeOf(PortTypedModel)).call(this, isInput, name, label, id));

    _this.type = type;
    return _this;
  }
  //


  _createClass(PortTypedModel, [{
    key: "serialize",
    value: function serialize() {
      return _.merge(_get(PortTypedModel.prototype.__proto__ || Object.getPrototypeOf(PortTypedModel.prototype), "serialize", this).call(this), {
        type: this.type
      });
    }
  }, {
    key: "deSerialize",
    value: function deSerialize(data) {
      _get(PortTypedModel.prototype.__proto__ || Object.getPrototypeOf(PortTypedModel.prototype), "deSerialize", this).call(this, data);
      this.type = data.type;
    }
  }]);

  return PortTypedModel;
}(SRD.DefaultPortModel);

var TypedPortWidget = exports.TypedPortWidget = function (_SRD$PortWidget) {
  _inherits(TypedPortWidget, _SRD$PortWidget);

  function TypedPortWidget(props) {
    _classCallCheck(this, TypedPortWidget);

    var _this2 = _possibleConstructorReturn(this, (TypedPortWidget.__proto__ || Object.getPrototypeOf(TypedPortWidget)).call(this, props));

    _this2.state = {
      selected: false
    };
    return _this2;
  }

  _createClass(TypedPortWidget, [{
    key: "render",
    value: function render() {
      var _this3 = this;

      var portClass = 'port ';

      if (this.state.selected) {
        portClass += 'selected ';
      }
      return React.DOM.div({
        onMouseEnter: function onMouseEnter() {
          _this3.setState({ selected: true });
        },
        onMouseLeave: function onMouseLeave() {
          _this3.setState({ selected: false });
        },
        key: this.props.id,
        className: portClass,
        'data-name': this.props.model.name,
        'data-nodeid': this.props.node.getID()
      });
    }
  }]);

  return TypedPortWidget;
}(SRD.PortWidget);

var TypedPortLabel = exports.TypedPortLabel = function (_React$Component) {
  _inherits(TypedPortLabel, _React$Component);

  function TypedPortLabel() {
    _classCallCheck(this, TypedPortLabel);

    return _possibleConstructorReturn(this, (TypedPortLabel.__proto__ || Object.getPrototypeOf(TypedPortLabel)).apply(this, arguments));
  }

  _createClass(TypedPortLabel, [{
    key: "render",
    value: function render() {
      var port = React.createElement(TypedPortWidget, { model: this.props.model, name: this.props.model.name, node: this.props.model.getParent() });
      var label = React.DOM.div({ className: 'name' }, [React.DOM.div({
        className: 'label name',
        key: 'a'
      }, this.props.model.label), React.DOM.div({
        className: 'typelabel name',
        key: 'b'
      }, ' [' + typesSigns[this.props.model.type] + ']')]);

      return React.DOM.div({ className: (this.props.model.in ? 'in' : 'out') + '-port' }, this.props.model.in ? port : label, this.props.model.in ? label : port);
    }
  }]);

  return TypedPortLabel;
}(React.Component);

var PortTypedFactory = exports.PortTypedFactory = function (_SRD$AbstractInstance) {
  _inherits(PortTypedFactory, _SRD$AbstractInstance);

  function PortTypedFactory() {
    _classCallCheck(this, PortTypedFactory);

    return _possibleConstructorReturn(this, (PortTypedFactory.__proto__ || Object.getPrototypeOf(PortTypedFactory)).call(this, "PortTypedModel"));
  }

  _createClass(PortTypedFactory, [{
    key: "getInstance",
    value: function getInstance() {
      return new PortTypedModel();
    }
  }]);

  return PortTypedFactory;
}(SRD.AbstractInstanceFactory);
//# sourceMappingURL=portTypedModel.js.map