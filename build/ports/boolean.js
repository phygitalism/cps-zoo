"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.BooleanPortFactory = exports.BooleanPortModel = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _stormReactDiagrams = require("storm-react-diagrams");

var SRD = _interopRequireWildcard(_stormReactDiagrams);

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _portTypedModel = require("./portTypedModel");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BooleanPortModel = exports.BooleanPortModel = function (_PortTypedModel) {
	_inherits(BooleanPortModel, _PortTypedModel);

	function BooleanPortModel(type, isInput, name) {
		var label = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'port';

		_classCallCheck(this, BooleanPortModel);

		return _possibleConstructorReturn(this, (BooleanPortModel.__proto__ || Object.getPrototypeOf(BooleanPortModel)).call(this, type, isInput, name, label));
	}

	return BooleanPortModel;
}(_portTypedModel.PortTypedModel);

var BooleanPortFactory = exports.BooleanPortFactory = function (_SRD$AbstractInstance) {
	_inherits(BooleanPortFactory, _SRD$AbstractInstance);

	function BooleanPortFactory() {
		_classCallCheck(this, BooleanPortFactory);

		return _possibleConstructorReturn(this, (BooleanPortFactory.__proto__ || Object.getPrototypeOf(BooleanPortFactory)).call(this, "BooleanPortModel"));
	}

	_createClass(BooleanPortFactory, [{
		key: "getInstance",
		value: function getInstance() {
			return new BooleanPortModel();
		}
	}]);

	return BooleanPortFactory;
}(SRD.AbstractInstanceFactory);
//# sourceMappingURL=boolean.js.map