'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _boolean = require('./boolean');

var _number = require('./number');

exports.default = {
  boolean: _boolean.BooleanPortModel,
  double: _number.NumberPortModel
};
//# sourceMappingURL=index.js.map