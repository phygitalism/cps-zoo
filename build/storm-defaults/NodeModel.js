'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TypedNodeModel = exports.TypedNodeInstanceFactory = undefined;

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _stormReactDiagrams = require('storm-react-diagrams');

var SRD = _interopRequireWildcard(_stormReactDiagrams);

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _portTypedModel = require('../ports/portTypedModel');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NodeModel = SRD.NodeModel;
var AbstractInstanceFactory = SRD.AbstractInstanceFactory;

var TypedNodeInstanceFactory = exports.TypedNodeInstanceFactory = function (_AbstractInstanceFact) {
  _inherits(TypedNodeInstanceFactory, _AbstractInstanceFact);

  function TypedNodeInstanceFactory(a, b) {
    _classCallCheck(this, TypedNodeInstanceFactory);

    console.log('TypedNodeInstanceFactory++ ', a, b);
    return _possibleConstructorReturn(this, (TypedNodeInstanceFactory.__proto__ || Object.getPrototypeOf(TypedNodeInstanceFactory)).call(this, "TypedNodeModel"));
  }

  _createClass(TypedNodeInstanceFactory, [{
    key: 'getInstance',
    value: function getInstance(initialConfig) {
      console.log('TypedNodeInstanceFactory', initialConfig);
      return new TypedNodeModel(initialConfig);
    }
  }]);

  return TypedNodeInstanceFactory;
}(AbstractInstanceFactory);

/**
 * @author Dylan Vorster
 */

var TypedNodeModel = exports.TypedNodeModel = function (_NodeModel) {
  _inherits(TypedNodeModel, _NodeModel);

  function TypedNodeModel(config) {
    _classCallCheck(this, TypedNodeModel);

    var _this2 = _possibleConstructorReturn(this, (TypedNodeModel.__proto__ || Object.getPrototypeOf(TypedNodeModel)).call(this, config.type));

    _this2.name = config.type;
    _this2.x = config.x;
    _this2.y = config.y;
    _this2.data = {};
    _this2.id = config.id;
    _this2.color = config.color || 'rgb(70,70,80)';
    _this2.activitySignal = false;
    _this2.activityTimeout;
    _this2.extras = config.extras;
    _this2.config = config;
    if (!_.isEmpty(config.ports.in) || !_.isEmpty(config.ports.out)) {
      _.map(config.ports.in, function (port) {
        _this2.addPort(new _portTypedModel.PortTypedModel(port.type, true, port.name, port.label, port.id));
      });

      _.map(config.ports.out, function (port) {
        _this2.addPort(new _portTypedModel.PortTypedModel(port.type, false, port.name, port.label, port.id));
      });
    } else {
      _.map(config.ports, function (port) {
        _this2.addPort(new _portTypedModel.PortTypedModel(port.type, port.in, port.name, port.label, port.id));
      });
    }
    return _this2;
  }

  _createClass(TypedNodeModel, [{
    key: 'setData',
    value: function setData(obj) {
      Object.assign(this.data, obj);
    }
  }, {
    key: 'setActivity',
    value: function setActivity(forceUpdate) {
      var _this3 = this;

      this.activitySignal = true;
      if (forceUpdate) {
        forceUpdate();
      }
      if (this.activityTimeout) {
        clearTimeout(this.activityTimeout);
      }
      this.activityTimeout = setTimeout(function () {
        _this3.activitySignal = false;
        if (forceUpdate) {
          forceUpdate();
        }
      }, 100);
    }
  }, {
    key: 'deSerialize',
    value: function deSerialize(object) {
      _get(TypedNodeModel.prototype.__proto__ || Object.getPrototypeOf(TypedNodeModel.prototype), 'deSerialize', this).call(this, object);
      this.name = object.name;
      this.color = object.color;
    }
  }, {
    key: 'serialize',
    value: function serialize() {
      return _.merge(_get(TypedNodeModel.prototype.__proto__ || Object.getPrototypeOf(TypedNodeModel.prototype), 'serialize', this).call(this), {
        name: this.name,
        color: this.color
      });
    }
  }, {
    key: 'getInPorts',
    value: function getInPorts() {
      return _.filter(this.ports, function (portModel) {
        return portModel.in;
      });
    }
  }, {
    key: 'getOutPorts',
    value: function getOutPorts() {
      return _.filter(this.ports, function (portModel) {
        return !portModel.in;
      });
    }
  }]);

  return TypedNodeModel;
}(NodeModel);
//# sourceMappingURL=NodeModel.js.map