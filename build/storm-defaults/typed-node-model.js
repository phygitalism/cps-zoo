"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TypedNodeModel = exports.TypedNodeInstanceFactory = undefined;

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _stormReactDiagrams = require("storm-react-diagrams");

var SRD = _interopRequireWildcard(_stormReactDiagrams);

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NodeModel = SRD.NodeModel;
var AbstractInstanceFactory = SRD.AbstractInstanceFactory;

var TypedNodeInstanceFactory = exports.TypedNodeInstanceFactory = function (_AbstractInstanceFact) {
  _inherits(TypedNodeInstanceFactory, _AbstractInstanceFact);

  function TypedNodeInstanceFactory() {
    _classCallCheck(this, TypedNodeInstanceFactory);

    return _possibleConstructorReturn(this, (TypedNodeInstanceFactory.__proto__ || Object.getPrototypeOf(TypedNodeInstanceFactory)).call(this, "TypedNodeModel"));
  }

  _createClass(TypedNodeInstanceFactory, [{
    key: "getInstance",
    value: function getInstance() {
      return new TypedNodeModel();
    }
  }]);

  return TypedNodeInstanceFactory;
}(AbstractInstanceFactory);

/**
 * @author Dylan Vorster
 */

var TypedNodeModel = exports.TypedNodeModel = function (_NodeModel) {
  _inherits(TypedNodeModel, _NodeModel);

  function TypedNodeModel(type) {
    _classCallCheck(this, TypedNodeModel);

    var _this2 = _possibleConstructorReturn(this, (TypedNodeModel.__proto__ || Object.getPrototypeOf(TypedNodeModel)).call(this, type));

    _this2.name = type;
    _this2.data = {};
    _this2.color = 'rgb(70,70,80)';
    _this2.activitySignal = false;
    _this2.activityTimeout;
    return _this2;
  }

  _createClass(TypedNodeModel, [{
    key: "setData",
    value: function setData(obj) {
      Object.assign(this.data, obj);
    }
  }, {
    key: "setActivity",
    value: function setActivity(forceUpdate) {
      var _this3 = this;

      this.activitySignal = true;
      if (forceUpdate) {
        forceUpdate();
      }
      if (this.activityTimeout) {
        clearTimeout(this.activityTimeout);
      }
      this.activityTimeout = setTimeout(function () {
        _this3.activitySignal = false;
        if (forceUpdate) {
          forceUpdate();
        }
      }, 100);
    }
  }, {
    key: "deSerialize",
    value: function deSerialize(object) {
      _get(TypedNodeModel.prototype.__proto__ || Object.getPrototypeOf(TypedNodeModel.prototype), "deSerialize", this).call(this, object);
      this.name = object.name;
      this.color = object.color;
    }
  }, {
    key: "serialize",
    value: function serialize() {
      return _.merge(_get(TypedNodeModel.prototype.__proto__ || Object.getPrototypeOf(TypedNodeModel.prototype), "serialize", this).call(this), {
        name: this.name,
        color: this.color
      });
    }
  }, {
    key: "getInPorts",
    value: function getInPorts() {
      return _.filter(this.ports, function (portModel) {
        return portModel.in;
      });
    }
  }, {
    key: "getOutPorts",
    value: function getOutPorts() {
      return _.filter(this.ports, function (portModel) {
        return !portModel.in;
      });
    }
  }]);

  return TypedNodeModel;
}(NodeModel);
//# sourceMappingURL=typed-node-model.js.map