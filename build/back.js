/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 17);
/******/ })
/************************************************************************/
/******/ ({

/***/ 17:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _back = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"../../AbctractSDS/back.js\""); e.code = 'MODULE_NOT_FOUND';; throw e; }()));

var _back2 = _interopRequireDefault(_back);

var _back3 = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"../../../Devices/CPS_Devices/CPS_LightSensor/back.js\""); e.code = 'MODULE_NOT_FOUND';; throw e; }()));

var _back4 = _interopRequireDefault(_back3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var config = __webpack_require__(6);
var dgram = __webpack_require__(7);

var SDS_CPS_LightSensor = function (_SDS) {
    _inherits(SDS_CPS_LightSensor, _SDS);

    function SDS_CPS_LightSensor() {
        _classCallCheck(this, SDS_CPS_LightSensor);

        var _this = _possibleConstructorReturn(this, (SDS_CPS_LightSensor.__proto__ || Object.getPrototypeOf(SDS_CPS_LightSensor)).call(this));

        _this.udp_Socket = dgram.createSocket('udp4');
        return _this;
    }
    //начать поиск датчиков в сети 


    _createClass(SDS_CPS_LightSensor, [{
        key: 'startSearching',
        value: function startSearching() {
            var _this2 = this;

            setInterval(function () {
                _this2.udp_Socket.send(config.MSG_HI_TO_SENSOR, config.UDP_PORT, config.BROADCAST_IP);
            }, config.BROADCAST_FREQ);

            this.udp_Socket.on('message', function (msg, rinfo) {
                if (msg.toString() == config.MSG_HI_FROM_SENSOR) {
                    for (var i = 0; i < _this2._availableDevices.length; i++) {
                        if (_this2._availableDevices[i].getHost() == rinfo.address) return;
                    }

                    var sensor = new _back4.default(_this2, rinfo.address);
                    _this2._availableDevices.push(sensor);
                }
            });
        }
        //остановить поиск лампочек 

    }, {
        key: 'stopSearching',
        value: function stopSearching() {
            this.udp_Socket.close();
            _get(SDS_CPS_LightSensor.prototype.__proto__ || Object.getPrototypeOf(SDS_CPS_LightSensor.prototype), 'stopSearching', this).call(this);
        }
    }, {
        key: 'sendRunToSensor',
        value: function sendRunToSensor(sensor) {
            this.udp_Socket.send(config.MSG_RUN, config.UDP_PORT, sensor.getHost());
        }
    }]);

    return SDS_CPS_LightSensor;
}(_back2.default);

exports.default = SDS_CPS_LightSensor;

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports = {
	"MSG_HI_TO_SENSOR": "hey",
	"MSG_HI_FROM_SENSOR": "Sensor",
	"BROADCAST_IP": "192.168.88.255",
	"UDP_PORT": "5002",
	"BROADCAST_FREQ": "1000",
	"MSG_RUN": "run"
};

/***/ }),

/***/ 7:
/***/ (function(module, exports) {

module.exports = require("dgram");

/***/ })

/******/ });