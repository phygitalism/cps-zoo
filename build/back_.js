/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 18);
/******/ })
/************************************************************************/
/******/ ({

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _back = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"../../AbstractDevice/back.js\""); e.code = 'MODULE_NOT_FOUND';; throw e; }()));

var _back2 = _interopRequireDefault(_back);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var config = __webpack_require__(6);
var http = __webpack_require__(4);

var CPS_LightSensor = function (_Device) {
    _inherits(CPS_LightSensor, _Device);

    function CPS_LightSensor(sds, host) {
        _classCallCheck(this, CPS_LightSensor);

        return _possibleConstructorReturn(this, (CPS_LightSensor.__proto__ || Object.getPrototypeOf(CPS_LightSensor)).call(this, sds, host));
    }
    //начать работу с датчиком света 


    _createClass(CPS_LightSensor, [{
        key: 'run',
        value: function run() {
            this._sds.sendRunToSensor(this);
            _get(CPS_LightSensor.prototype.__proto__ || Object.getPrototypeOf(CPS_LightSensor.prototype), 'run', this).call(this);
        }
        //получить уровень освещенности 

    }, {
        key: 'getLightValue',
        value: function getLightValue() {
            var command = 'http://' + this.getHost() + config.LIGHT_URI;

            var strData = '';

            http.get(command, function (res) {

                res.on('data', function (chunk) {
                    strData += chunk;
                });

                res.on('end', function () {
                    try {
                        return parseInt(strData);
                    } catch (e) {
                        console.log(e.message);
                    }
                });
            }).on('error', function () {});
        }
        //закончить работу с датчиком света 

    }, {
        key: 'stop',
        value: function stop() {
            var command = 'http://' + this.getHost() + config.STOP;
            http.get(command).on('error', function (err) {});
            _get(CPS_LightSensor.prototype.__proto__ || Object.getPrototypeOf(CPS_LightSensor.prototype), 'stop', this).call(this);
            // this = null ;
        }
    }]);

    return CPS_LightSensor;
}(_back2.default);

exports.default = CPS_LightSensor;

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = http;

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports = {
	"MSG_HI_TO_SENSOR": "hey",
	"MSG_HI_FROM_SENSOR": "Sensor",
	"BROADCAST_IP": "192.168.88.255",
	"UDP_PORT": "5002",
	"BROADCAST_FREQ": "1000",
	"MSG_RUN": "run"
};

/***/ })

/******/ });