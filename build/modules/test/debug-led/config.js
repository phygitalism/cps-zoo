'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  type: 'debug-led',
  extras: {},
  ports: {
    in: [{
      type: 'boolean',
      name: 'in0',
      label: 'in',
      desc: 'Led in'
    }]
  }
};
//# sourceMappingURL=config.js.map