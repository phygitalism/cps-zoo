"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DebugLedNodeWidgetFactory = exports.DebugLedNodeWidget = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var React = _interopRequireWildcard(_react);

var _stormReactDiagrams = require("storm-react-diagrams");

var SRD = _interopRequireWildcard(_stormReactDiagrams);

var _const = require("../../../../const");

var _const2 = _interopRequireDefault(_const);

var _config = require("../config");

var _config2 = _interopRequireDefault(_config);

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _portTypedModel = require("../../../../ports/portTypedModel");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var div = React.DOM.div;

/**
 * @author Dylan Vorster
 */
var DebugLedNodeWidget = exports.DebugLedNodeWidget = function (_React$Component) {
  _inherits(DebugLedNodeWidget, _React$Component);

  function DebugLedNodeWidget(props) {
    _classCallCheck(this, DebugLedNodeWidget);

    var _this = _possibleConstructorReturn(this, (DebugLedNodeWidget.__proto__ || Object.getPrototypeOf(DebugLedNodeWidget)).call(this, props));

    _this.state = {};
    return _this;
  }

  _createClass(DebugLedNodeWidget, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var ledClass = 'led-widget ';
      if (this.props.node.data.on) {
        ledClass += ' active';
      }
      return React.DOM.div({ className: 'basic-node ' + _config2.default.type + '-node', style: { position: 'relative', width: this.props.size,
          background: this.props.node.color || _const2.default.defaultBGColor,
          height: this.props.size } }, div({ className: 'title' }, div({ className: 'name' }, this.props.node.name), div({ className: 'activity ' + (this.props.node.activitySignal ? 'active' : '') }, '')), div({ className: ledClass }), div({ className: 'ports' }, div({ className: 'in' }, _.map(this.props.node.getInPorts(), function (port) {
        return React.createElement(_portTypedModel.TypedPortLabel, { model: port, node: _this2.props.node, key: port.id });
      })), div({ className: 'out' }, _.map(this.props.node.getOutPorts(), function (port) {
        return React.createElement(_portTypedModel.TypedPortLabel, { model: port, node: _this2.props.node, key: port.id });
      }))));
    }
  }]);

  return DebugLedNodeWidget;
}(React.Component);

var DebugLedNodeWidgetFactory = exports.DebugLedNodeWidgetFactory = React.createFactory(DebugLedNodeWidget);
//# sourceMappingURL=NodeWidget.js.map