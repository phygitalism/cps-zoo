'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DebugLedNodeModel = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _NodeModel = require('../../../../storm-defaults/NodeModel');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DebugLedNodeModel = exports.DebugLedNodeModel = function (_TypedNodeModel) {
  _inherits(DebugLedNodeModel, _TypedNodeModel);

  function DebugLedNodeModel(config) {
    _classCallCheck(this, DebugLedNodeModel);

    return _possibleConstructorReturn(this, (DebugLedNodeModel.__proto__ || Object.getPrototypeOf(DebugLedNodeModel)).call(this, config));
  }

  _createClass(DebugLedNodeModel, [{
    key: 'setData',
    value: function setData(msg) {
      this.data.on = String(msg) == String(true);
    }
  }]);

  return DebugLedNodeModel;
}(_NodeModel.TypedNodeModel);
//# sourceMappingURL=NodeModel.js.map