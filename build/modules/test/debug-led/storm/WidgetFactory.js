"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.WidgetFactory = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _config = require("../config.js");

var _config2 = _interopRequireDefault(_config);

var _stormReactDiagrams = require("storm-react-diagrams");

var SRD = _interopRequireWildcard(_stormReactDiagrams);

var _NodeWidget = require("./NodeWidget");

var _react = require("react");

var React = _interopRequireWildcard(_react);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var WidgetFactory = exports.WidgetFactory = function (_SRD$NodeWidgetFactor) {
	_inherits(WidgetFactory, _SRD$NodeWidgetFactor);

	function WidgetFactory() {
		_classCallCheck(this, WidgetFactory);

		return _possibleConstructorReturn(this, (WidgetFactory.__proto__ || Object.getPrototypeOf(WidgetFactory)).call(this, _config2.default.type));
	}

	_createClass(WidgetFactory, [{
		key: "generateReactWidget",
		value: function generateReactWidget(diagramEngine, node) {
			return React.createElement(_NodeWidget.DebugLedNodeWidgetFactory, { node: node, diagramEngine: diagramEngine });
		}
	}]);

	return WidgetFactory;
}(SRD.NodeWidgetFactory);
//# sourceMappingURL=WidgetFactory.js.map