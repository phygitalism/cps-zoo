'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _testDeviceModule = require('./test-device-module');

var _testDeviceModule2 = _interopRequireDefault(_testDeviceModule);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  TestDevice: _testDeviceModule2.default
};
//# sourceMappingURL=index.js.map