'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _cpsModule = require('cps-module');

var _cpsModule2 = _interopRequireDefault(_cpsModule);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _dgram = require('dgram');

var _dgram2 = _interopRequireDefault(_dgram);

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var log = (0, _debug2.default)('driver:test-device');

var Back = function (_CpsModule) {
  _inherits(Back, _CpsModule);

  function Back(id, transport, params) {
    _classCallCheck(this, Back);

    var _this = _possibleConstructorReturn(this, (Back.__proto__ || Object.getPrototypeOf(Back)).call(this, id, transport, _config2.default, params));

    _this.app = (0, _express2.default)();
    _this.clients = [];
    _this.getPort().then(function (port) {
      return _this.runHttpServer.bind(_this)(port);
    }).then(_this.sendUDP).catch(function (err) {
      console.error(err);
    });
    return _this;
  }

  _createClass(Back, [{
    key: 'runHttpServer',
    value: function runHttpServer(port) {
      var _this2 = this;

      return new _bluebird2.default(function (resolve, reject) {

        _this2.app.get('/reg', function (req, res) {
          log('/reg', req.query, req.hostname);
          if (req.query.name == _this2.options.ext.regToken) {
            _this2.clients.push(req.hostname);
          }
          return res.status(204).send();
        });

        _this2.app.get('/:cmd', function (req, res) {
          log('/:cmd', req.query);
          if (req.query.light) {
            console.log('light');
            if (req.query.value) {
              _this2.sendAll(req.params.cmd, req.query.value < 600 && req.query.value > 200);
            }
          }
        });

        _this2.app.listen(port || _this2.options.ext.HTTP.PORT);
        log('http server started');
        resolve(null);
      });
    }
  }, {
    key: 'sendAll',
    value: function sendAll(cmd, onOff) {
      var onOffStr = onOff ? '0' : '1';
      log('Sending data to MC');
      this.clients.forEach(function (client) {
        _request2.default.get('http://' + client + '/?name=1&number=13&pin=Digital&mode=OUT&oldlevel=' + onOffStr).on('response', function (response) {
          log('Sending data response', response);
        }).on('error', function (err) {
          log('Sending data error', err);
        });
      });
    }
  }, {
    key: 'startUDPSending',
    value: function startUDPSending() {
      var _this3 = this;

      setInterval(function () {
        udp.send(_this3.options.ext.UDP.MSG, _this3.options.ext.UDP.PORT, _this3.options.ext.UDP.URL, function (err, res) {
          if (err) {
            return log('error: ', err);
          }
          log('response: ', res);
        });
      }, this.options.ext.UDP.BROADCAST_FREQ_MS);
    }
  }, {
    key: 'sendUDP',
    value: function sendUDP() {
      var _this4 = this;

      return new _bluebird2.default(function (resolve, reject) {
        var udp = _dgram2.default.createSocket('udp4');

        _this4.startUDPSending();

        log('udp broadcast started');
        resolve(null);
      });
    }
  }]);

  return Back;
}(_cpsModule2.default);

exports.default = Back;
//# sourceMappingURL=server-model.js.map