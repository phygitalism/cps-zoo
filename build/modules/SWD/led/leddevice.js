'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _abstractDevice = require('../common/abstractDevice');

var _abstractDevice2 = _interopRequireDefault(_abstractDevice);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var http = require('http');

var CPS_Led = function (_Device) {
    _inherits(CPS_Led, _Device);

    function CPS_Led(sds, host, config) {
        _classCallCheck(this, CPS_Led);

        var _this = _possibleConstructorReturn(this, (CPS_Led.__proto__ || Object.getPrototypeOf(CPS_Led)).call(this, sds, host));

        _this.config = config;
        return _this;
    }
    //начало работы с лампочкой 


    _createClass(CPS_Led, [{
        key: 'run',
        value: function run() {
            this._sds.sendRunToLed(this);
            _get(CPS_Led.prototype.__proto__ || Object.getPrototypeOf(CPS_Led.prototype), 'run', this).call(this);
        }
        //зажечь первую лампочку 

    }, {
        key: 'firstLedOn',
        value: function firstLedOn() {
            var command = 'http://' + this.getHost() + this.config.FirstLedOn.value;
            http.get(command).on('error', function (err) {});
        }
        //погасить первую лампочку 

    }, {
        key: 'firstLedOff',
        value: function firstLedOff() {
            var command = 'http://' + this.getHost() + this.config.FirstLedOff.value;
            http.get(command).on('error', function (err) {});
        }
        //прекратить работу с лампчкой  

    }, {
        key: 'stop',
        value: function stop() {
            var command = 'http://' + this.getHost() + this.config.Stop.value;
            http.get(command).on('error', function (err) {});
            _get(CPS_Led.prototype.__proto__ || Object.getPrototypeOf(CPS_Led.prototype), 'stop', this).call(this);
            // this = null ;
        }
    }]);

    return CPS_Led;
}(_abstractDevice2.default);

exports.default = CPS_Led;
//# sourceMappingURL=leddevice.js.map