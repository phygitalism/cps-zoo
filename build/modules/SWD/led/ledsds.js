'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _abstractSDS = require('../common/abstractSDS.js');

var _abstractSDS2 = _interopRequireDefault(_abstractSDS);

var _leddevice = require('./leddevice');

var _leddevice2 = _interopRequireDefault(_leddevice);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var dgram = require('dgram');

var SDS_CPS_Led = function (_SDS) {
    _inherits(SDS_CPS_Led, _SDS);

    function SDS_CPS_Led(config) {
        _classCallCheck(this, SDS_CPS_Led);

        var _this = _possibleConstructorReturn(this, (SDS_CPS_Led.__proto__ || Object.getPrototypeOf(SDS_CPS_Led)).call(this));

        _this.config = config;
        console.log(' >>> ', config);
        _this.udp_Socket = dgram.createSocket('udp4');
        return _this;
    }
    //начать поиск лампочек в сети 


    _createClass(SDS_CPS_Led, [{
        key: 'startSearching',
        value: function startSearching() {
            // setInterval(() => {
            //     console.log(' .. sending to ',this.config.MSG_HI_TO_LED.value,
            //       this.config.UDP_PORT.value,
            //       this.config.BROADCAST_IP.value,":", this.config.BROADCAST_FREQ.value)
            //     this.udp_Socket.send(this.config.MSG_HI_TO_LED.value,
            //       this.config.UDP_PORT.value,
            //       this.config.BROADCAST_IP.value);
            // }, this.config.BROADCAST_FREQ.value);
            //
            // this.udp_Socket.on('message', (msg, rinfo)=> {
            //     if (msg.toString() == this.config.MSG_HI_FROM_LED.value)
            //     {
            //         for (var i = 0; i < this._availableDevices.length; i ++ )
            //         {
            //           if (this._availableDevices[i] == rinfo.address)
            //             return;
            //             if (this._availableDevices[i].getHost() == rinfo.address)
            //                 return;
            //         }
            //
            //         var led = new LedDevice(this, rinfo.address, this.config);
            //         this._availableDevices.push(led);
            //     }
            // });
            var led = new _leddevice2.default(this, '192.168.88.109', this.config);
            this._availableDevices.push(led);
        }
        //остановить поиск лампочек 

    }, {
        key: 'stopSearching',
        value: function stopSearching() {
            this.udp_Socket.close();
            _get(SDS_CPS_Led.prototype.__proto__ || Object.getPrototypeOf(SDS_CPS_Led.prototype), 'stopSearching', this).call(this);
        }
    }, {
        key: 'sendRunToLed',
        value: function sendRunToLed(led) {
            this.udp_Socket.send('run', this.config.UDP_PORT.value, led.getHost());
        }
    }]);

    return SDS_CPS_Led;
}(_abstractSDS2.default);

exports.default = SDS_CPS_Led;
//# sourceMappingURL=ledsds.js.map