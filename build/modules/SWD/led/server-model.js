'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _cpsModule = require('cps-module');

var _cpsModule2 = _interopRequireDefault(_cpsModule);

var _ledsds = require('./ledsds');

var _ledsds2 = _interopRequireDefault(_ledsds);

var _leddevice = require('./leddevice');

var _leddevice2 = _interopRequireDefault(_leddevice);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// 1) экзем LedSDS
// 2) startSearching (вызвать сразу)
// 3) getAvailable devices
// 4) device.getHost
// 5) device.run


var Back = function (_CpsModule) {
  _inherits(Back, _CpsModule);

  function Back(id, transport, config) {
    _classCallCheck(this, Back);

    var _this = _possibleConstructorReturn(this, (Back.__proto__ || Object.getPrototypeOf(Back)).call(this, id, transport, config));

    _this.device = {};
    var sds = new _ledsds2.default(_this.params);
    sds.startSearching();
    var interval = setInterval(function () {
      var devicesList = sds.getAvailableDevices();
      _this.device = devicesList[0];
      console.log('+ device', _this.device);
      if (_this.device) {
        _this.device.run();
        clearInterval(interval);
      }
    }, 3000);

    _this.onSignal(function (channel, message) {
      if (_this.device) {
        if (message) {
          _this.device.firstLedOn();
        } else {
          _this.device.firstLedOff();
        }
      }
    });
    return _this;
  }

  return Back;
}(_cpsModule2.default);

exports.default = Back;
//# sourceMappingURL=server-model.js.map