'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LedNodeModel = undefined;

var _NodeModel = require('../../../../storm-defaults/NodeModel');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LedNodeModel = exports.LedNodeModel = function (_TypedNodeModel) {
  _inherits(LedNodeModel, _TypedNodeModel);

  function LedNodeModel(config) {
    _classCallCheck(this, LedNodeModel);

    return _possibleConstructorReturn(this, (LedNodeModel.__proto__ || Object.getPrototypeOf(LedNodeModel)).call(this, config));
    // this.extras = CONFIG.extras;
    // _.map(CONFIG.ports.in, (port) => {
    //   this.addPort(new PortTypedModel(port.type, true, port.name, port.label));
    // });
    //
    // _.map(CONFIG.ports.out, (port) => {
    //   this.addPort(new PortTypedModel(port.type, false, port.name, port.label));
    // });
  }

  return LedNodeModel;
}(_NodeModel.TypedNodeModel);
//# sourceMappingURL=NodeModel.js.map