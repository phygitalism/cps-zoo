"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Device = function () {
	//для обратной связи нужно в конструктор передавать instanse SDS 
	//который обнаружил данное устойство 
	//и host этого устройства 
	function Device(sds, host) {
		_classCallCheck(this, Device);

		this._sds = sds;
		this._host = host;
	}

	_createClass(Device, [{
		key: "getHost",
		value: function getHost() {
			return this._host;
		}
		//метод run поставляется для расширения дочерними класса 
		//необходимо перевести устройство в рабочее состояние (реализуется в конкретных классах устройств) 
		//и удалить из списка доступных устройств в ассоциированном sds 

	}, {
		key: "run",
		value: function run() {
			this._sds.removeAvailable(this);
		}
		//метод без реализации для оставновки устройства и перевода его в состояние ожидания

	}, {
		key: "stop",
		value: function stop() {}
	}]);

	return Device;
}();

exports.default = Device;
//# sourceMappingURL=abstractDevice.js.map