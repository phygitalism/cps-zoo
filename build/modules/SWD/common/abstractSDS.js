"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SDS = function () {
	function SDS() {
		_classCallCheck(this, SDS);

		//массив найденых устройств в сети 
		this._availableDevices = [];
	}
	//метод без реализации определен только для расширения
	//если метод запущен то в this._availableDevices добавляются устройства которые откликнулись  


	_createClass(SDS, [{
		key: "startSearching",
		value: function startSearching() {}
		//метод без реализации определен только для расширения 

	}, {
		key: "stopSearching",
		value: function stopSearching() {
			this._availableDevices = [];
		}
		//метод с реализацией возращает список доступных устройств 

	}, {
		key: "getAvailableDevices",
		value: function getAvailableDevices() {
			return this._availableDevices;
		}
		//метод удаляющий устройство из списка доступных 

	}, {
		key: "removeAvailable",
		value: function removeAvailable(device) {
			//ищем индекс элемента с хостом host 
			var _index = this._availableDevices.indexOf(device);
			this._availableDevices.splice(_index, 1);
		}
	}]);

	return SDS;
}();

exports.default = SDS;
//# sourceMappingURL=abstractSDS.js.map