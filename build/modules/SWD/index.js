'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _led = require('./led');

var _led2 = _interopRequireDefault(_led);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  Leddevice: _led2.default
};
//# sourceMappingURL=index.js.map