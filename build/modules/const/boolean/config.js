'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  type: 'const_boolean',
  extras: {
    state: {
      type: 'boolean',
      label: 'state',
      isExternal: true,
      default: false
    }
  },
  ports: {
    out: [{
      type: 'boolean',
      name: 'out0',
      label: 'out',
      desc: 'Boolean out'
    }]
  }
};
//# sourceMappingURL=config.js.map