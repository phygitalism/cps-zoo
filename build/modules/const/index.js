'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _boolean = require('./boolean');

var _boolean2 = _interopRequireDefault(_boolean);

var _number = require('./number');

var _number2 = _interopRequireDefault(_number);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  ConstBoolean: _boolean2.default,
  ConstNumber: _number2.default
};
//# sourceMappingURL=index.js.map