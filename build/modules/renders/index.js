'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _unity = require('./unity');

var _unity2 = _interopRequireDefault(_unity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  Unity: _unity2.default
};
//# sourceMappingURL=index.js.map