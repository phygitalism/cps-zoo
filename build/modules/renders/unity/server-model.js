'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _cpsModule = require('cps-module');

var _cpsModule2 = _interopRequireDefault(_cpsModule);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// import config from './config'

var server = require('http').createServer();
var io = require('socket.io')(server);
var moduleAlreadyConfigurated = false;

var Back = function (_CpsModule) {
  _inherits(Back, _CpsModule);

  function Back(id, transport, config) {
    _classCallCheck(this, Back);

    var _this = _possibleConstructorReturn(this, (Back.__proto__ || Object.getPrototypeOf(Back)).call(this, id, transport, config));

    _this.sockets = [];

    io.on('connection', function (socket) {
      _this.sockets.push(socket);
      console.log('\n\n!!! \nuser connected');

      socket.on('signalOut', function (msg) {
        console.log('\n\nsignalOut >> ', msg);
        try {
          if (typeof msg == 'string') {
            msg = JSON.parse(msg);
          }
        } catch (e) {
          console.log('err', e);
        }
        _this.emitSignal(msg.portId, msg.data);
      });

      socket.on('initConfiguration', function (msg) {
        console.log('\n\n initConfiguration >> ', typeof msg === 'undefined' ? 'undefined' : _typeof(msg), msg);
        if (moduleAlreadyConfigurated) {
          return;
        }
        try {
          if (typeof msg == 'string') {
            msg = JSON.parse(msg);
          }
        } catch (e) {
          console.log('err', e);
        }
        console.log('2 initConfiguration >> ', typeof msg === 'undefined' ? 'undefined' : _typeof(msg));
        moduleAlreadyConfigurated = true;
        _this.updateConfig(msg);
      });
    });
    var ii = true;

    _this.onSignal(function (channel, message) {
      console.log(' >>> SIGNAL IN', channel, message);
      io.emit('signalIn', { portId: channel, data: message });
    });

    console.log('UNITY STARTED', _this.params.PORT.value);
    server.listen(_this.params.PORT.value || '6002');
    return _this;
  }

  return Back;
}(_cpsModule2.default);

exports.default = Back;
//# sourceMappingURL=server-model.js.map