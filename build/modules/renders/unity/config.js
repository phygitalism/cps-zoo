'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  type: 'unity',
  extras: {
    PORT: {
      type: 'port',
      isExternal: true,
      default: 6002,
      required: true
    }
  },
  ports: {
    in: [],
    out: []
  }
};
//# sourceMappingURL=config.js.map