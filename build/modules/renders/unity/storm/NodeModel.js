"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UnityNodeModel = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _stormReactDiagrams = require("storm-react-diagrams");

var SRD = _interopRequireWildcard(_stormReactDiagrams);

var _ports = require("../../../../ports");

var _ports2 = _interopRequireDefault(_ports);

var _config = require("../config.js");

var _config2 = _interopRequireDefault(_config);

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _portTypedModel = require("../../../../ports/portTypedModel");

var _typedNodeModel = require("../../../../storm-defaults/typed-node-model");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UnityNodeModel = exports.UnityNodeModel = function (_TypedNodeModel) {
  _inherits(UnityNodeModel, _TypedNodeModel);

  function UnityNodeModel() {
    _classCallCheck(this, UnityNodeModel);

    var _this = _possibleConstructorReturn(this, (UnityNodeModel.__proto__ || Object.getPrototypeOf(UnityNodeModel)).call(this, _config2.default.type));

    _this.extras = _config2.default.extras;
    _.map(_config2.default.ports.in, function (port) {
      _this.addPort(new _portTypedModel.PortTypedModel(port.type, true, port.name, port.label));
    });

    _.map(_config2.default.ports.out, function (port) {
      _this.addPort(new _portTypedModel.PortTypedModel(port.type, false, port.name, port.label));
    });

    // let ports = inports.concat(outports);
    // _.each(ports, (port) => {

    // });
    return _this;
  }

  _createClass(UnityNodeModel, [{
    key: "deSerialize",
    value: function deSerialize(newConf) {
      var _this2 = this;

      _get(UnityNodeModel.prototype.__proto__ || Object.getPrototypeOf(UnityNodeModel.prototype), "deSerialize", this).call(this, newConf);
      _.each(this.getPorts, function (port) {
        _this2.removePort(port);
      });
      _.map(newConf.ports, function (port) {
        _this2.addPort(new _portTypedModel.PortTypedModel(port.type, port.in, port.name, port.label, port.id));
      });
    }
  }]);

  return UnityNodeModel;
}(_typedNodeModel.TypedNodeModel);
//# sourceMappingURL=NodeModel.js.map