"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DelayNodeModel = undefined;

var _stormReactDiagrams = require("storm-react-diagrams");

var SRD = _interopRequireWildcard(_stormReactDiagrams);

var _ports = require("../../../../ports");

var _ports2 = _interopRequireDefault(_ports);

var _config = require("../config.js");

var _config2 = _interopRequireDefault(_config);

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _portTypedModel = require("../../../../ports/portTypedModel");

var _typedNodeModel = require("../../../../storm-defaults/typed-node-model");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DelayNodeModel = exports.DelayNodeModel = function (_TypedNodeModel) {
  _inherits(DelayNodeModel, _TypedNodeModel);

  function DelayNodeModel() {
    _classCallCheck(this, DelayNodeModel);

    var _this = _possibleConstructorReturn(this, (DelayNodeModel.__proto__ || Object.getPrototypeOf(DelayNodeModel)).call(this, _config2.default.type_id));

    _this.extras = _config2.default.extras;
    _.map(_config2.default.ports.in, function (port) {
      _this.addPort(new _portTypedModel.PortTypedModel(port.type, true, port.name, port.label));
    });

    _.map(_config2.default.ports.out, function (port) {
      _this.addPort(new _portTypedModel.PortTypedModel(port.type, false, port.name, port.label));
    });

    return _this;
  }

  return DelayNodeModel;
}(_typedNodeModel.TypedNodeModel);
//# sourceMappingURL=NodeModel.js.map