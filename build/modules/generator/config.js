'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  type_id: 'generator',
  extras: {
    signalType: {
      type: 'string',
      isExternal: true,
      enum: ['sin', 'sqr', 'tri'],
      default: 'sin'
    },
    fps: {
      type: 'number',
      isExternal: true,
      min: 0,
      default: 600
    },
    signalFreq: {
      type: 'number',
      isExternal: true,
      min: 0,
      default: 10
    },
    signalAmplitude: {
      type: 'number',
      isExternal: true,
      min: 0,
      default: 1
    }
  },
  ports: {
    out: [{
      type: 'number',
      name: 'generatorout',
      label: 'out',
      desc: 'Out port'
    }]
  }
};
//# sourceMappingURL=config.js.map