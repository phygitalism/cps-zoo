'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _cpsModule = require('cps-module');

var _cpsModule2 = _interopRequireDefault(_cpsModule);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Back = function (_CpsModule) {
  _inherits(Back, _CpsModule);

  function Back(id, transport, config) {
    _classCallCheck(this, Back);

    //функция выходного сигнала
    var _this = _possibleConstructorReturn(this, (Back.__proto__ || Object.getPrototypeOf(Back)).call(this, id, transport, config));

    var func = void 0;
    //момент времени, сначала 0
    var t = 0;
    //угловая частота
    var omega = Math.PI * 2 * _this.extras.signalFreq;
    //амплитуда сигнала
    var amplitude = _this.extras.signalAmplitude;
    //если в настройках тип сигнала sin
    if (_this.extras.signalType == 'sin') func = Math.sin;else func = Math.cos;

    setInterval(function () {
      //расчет выходного значения по формуле
      // U = A * sin(omega * t)
      var outValue = amplitude * func(omega * t);
      _this.emitSignal(_this.ports.out[0].id, outValue);
      t += 1 / _this.extras.fps; //if fps = 60
    }, _this.extras.fps * 1000);

    // this.onSignal((channel, message) => {
    // });
    return _this;
  }

  return Back;
}(_cpsModule2.default);

exports.default = Back;
//# sourceMappingURL=server-model.js.map