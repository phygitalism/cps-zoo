'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    type_id: 'not',
    extras: {},
    ports: {
        in: [{
            type: 'boolean',
            name: 'in1',
            label: 'in 1',
            desc: 'Input port 1'
        }],

        out: [{
            type: 'boolean',
            name: 'NOTout',
            label: 'out',
            desc: 'Out port'
        }]
    }
};
//# sourceMappingURL=config.js.map