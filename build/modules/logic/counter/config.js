'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    type_id: 'counter',
    extras: {
        counterModule: {
            type: 'number',
            isExternal: true,
            default: 10
        }
    },
    ports: {
        in: [{
            type: 'boolean',
            name: 'counterin',
            label: 'in',
            desc: 'Counter input'
        }],

        out: [{
            type: 'number',
            name: 'counterout',
            label: 'out',
            desc: 'Out port'
        }]
    }
};
//# sourceMappingURL=config.js.map