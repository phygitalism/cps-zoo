"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.CounterWidgetFactory = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _config = require("../config.js");

var _config2 = _interopRequireDefault(_config);

var _stormReactDiagrams = require("storm-react-diagrams");

var SRD = _interopRequireWildcard(_stormReactDiagrams);

var _NodeWidget = require("../../../../storm-defaults/NodeWidget");

var _react = require("react");

var React = _interopRequireWildcard(_react);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CounterWidgetFactory = exports.CounterWidgetFactory = function (_SRD$NodeWidgetFactor) {
	_inherits(CounterWidgetFactory, _SRD$NodeWidgetFactor);

	function CounterWidgetFactory() {
		_classCallCheck(this, CounterWidgetFactory);

		return _possibleConstructorReturn(this, (CounterWidgetFactory.__proto__ || Object.getPrototypeOf(CounterWidgetFactory)).call(this, _config2.default.type_id, _config2.default));
	}

	_createClass(CounterWidgetFactory, [{
		key: "generateReactWidget",
		value: function generateReactWidget(diagramEngine, node) {
			return React.createElement(_NodeWidget.TypedNodeWidgetFactory, { node: node, diagramEngine: diagramEngine, config: _config2.default });
		}
	}]);

	return CounterWidgetFactory;
}(SRD.NodeWidgetFactory);
//# sourceMappingURL=WidgetFactory.js.map