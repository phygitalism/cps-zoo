'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _delay = require('./delay');

var _delay2 = _interopRequireDefault(_delay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  Delay: _delay2.default
};
//# sourceMappingURL=index.js.map