'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    type_id: 'xor',
    extras: {},
    ports: {
        in: [{
            type: 'boolean',
            name: 'in1',
            label: 'in 1',
            desc: 'Input port 1'
        }, {
            type: 'boolean',
            name: 'in2',
            label: 'in 2',
            desc: 'Input port 2'
        }],

        out: [{
            type: 'boolean',
            name: 'ORout',
            label: 'out',
            desc: 'Out port'
        }]
    }
};
//# sourceMappingURL=config.js.map