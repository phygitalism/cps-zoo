'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  label: 'le matrix',
  id: 'matrix',
  binPath: ".\\bin\\RandomMatrix"

}, {
  label: 'le sinus',
  id: 'sinus',
  binPath: ".\\bin\\Sinus"
}];
//# sourceMappingURL=models-list.js.map