'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  type: 'template',
  extras: {
    model: {
      type: 'string',
      label: 'model type',
      isExternal: true,
      default: 'sinus.cs'
    }
  },
  ports: {
    in: [],
    out: []
  }
};
//# sourceMappingURL=config.js.map