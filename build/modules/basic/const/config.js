'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _cpsModule = require('cps-module');

var _index = require('../../../ports/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  type: 'basic/const',
  extra: {
    isFloat: {
      type: _cpsModule.TYPES.params.boolean,
      isExternal: true,
      default: false
    }
  },
  ports: {
    out: [{
      type: _index2.default.number,
      id: 'out0',
      name: 'out',
      desc: 'Number'
    }]
  }
};
//# sourceMappingURL=config.js.map