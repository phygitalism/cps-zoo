"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.ConstNodeWidgetFactory = exports.ConstNodeWidget = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var React = _interopRequireWildcard(_react);

var _NodeModel = require("./NodeModel");

var _stormReactDiagrams = require("storm-react-diagrams");

var SRD = _interopRequireWildcard(_stormReactDiagrams);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * @author Dylan Vorster
 */

var defaultProps = {
	size: 150,
	node: null
};

var ConstNodeWidget = exports.ConstNodeWidget = function (_React$Component) {
	_inherits(ConstNodeWidget, _React$Component);

	function ConstNodeWidget(props) {
		_classCallCheck(this, ConstNodeWidget);

		var _this = _possibleConstructorReturn(this, (ConstNodeWidget.__proto__ || Object.getPrototypeOf(ConstNodeWidget)).call(this, props));

		_this.state = {};
		return _this;
	}

	_createClass(ConstNodeWidget, [{
		key: "render",
		value: function render() {
			return React.DOM.div({ className: "diamond-node", style: { position: 'relative', width: this.props.size, height: this.props.size } }, React.DOM.svg({
				width: this.props.size, height: this.props.size, dangerouslySetInnerHTML: { __html: "\n\t\t\t\t\t\t<g id=\"Layer_1\">\n\t\t\t\t\t\t</g>\n\t\t\t\t\t\t<g id=\"Layer_2\">\n\t\t\t\t\t\t\t<polygon fill=\"cyan\" stroke=\"#000000\" stroke-width=\"3\" stroke-miterlimit=\"10\" points=\"10," + this.props.size / 2 + " " + this.props.size / 2 + ",10 " + (this.props.size - 10) + "," + this.props.size / 2 + " " + this.props.size / 2 + "," + (this.props.size - 10) + " \"/>\n\t\t\t\t\t\t</g>\n\t\t\t\t" } }),

			//left node
			React.DOM.div({ style: { position: 'absolute', zIndex: 10, top: this.props.size / 2 - 5 } }, React.createElement(SRD.PortWidget, { name: 'left', node: this.props.node })),

			//top node
			React.DOM.div({ style: { position: 'absolute', zIndex: 10, left: this.props.size / 2 - 8 } }, React.createElement(SRD.PortWidget, { name: 'top', node: this.props.node })),

			//right
			React.DOM.div({ style: { position: 'absolute', zIndex: 10, left: this.props.size - 10, top: this.props.size / 2 } }, React.createElement(SRD.PortWidget, { name: 'right', node: this.props.node })),

			//bottom
			React.DOM.div({ style: { position: 'absolute', zIndex: 10, left: this.props.size / 2 - 8, top: this.props.size - 10 } }, React.createElement(SRD.PortWidget, { name: 'bottom', node: this.props.node })));
		}
	}]);

	return ConstNodeWidget;
}(React.Component);

var ConstNodeWidgetFactory = exports.ConstNodeWidgetFactory = React.createFactory(ConstNodeWidget);
//# sourceMappingURL=NodeWidget.js.map