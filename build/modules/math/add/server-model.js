'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _cpsModule = require('cps-module');

var _cpsModule2 = _interopRequireDefault(_cpsModule);

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Back = function (_CpsModule) {
  _inherits(Back, _CpsModule);

  function Back(id, transport, config) {
    _classCallCheck(this, Back);

    var _this = _possibleConstructorReturn(this, (Back.__proto__ || Object.getPrototypeOf(Back)).call(this, id, transport, config));

    _this.updateConfig({ ports: [{
        type: 'double',
        name: 'in0',
        id: 'in0',
        label: 'in 0',
        in: true,
        desc: ''
      }, {
        type: 'double',
        name: 'in1',
        id: 'in1',
        label: 'in 1',
        in: true,
        desc: ''
      }, {
        type: 'double',
        name: 'out0',
        id: 'out0',
        label: 'out',
        in: false,
        desc: 'Sum of inputs'
      }]
    });
    _this.onSignal(function (portId, message) {
      // do something here
      var sum = _.reduce(_this.ports.in, function (total, port) {
        console.log('add port ', port);
        return total + (port.value || 0);
      }, 0);

      _this.emitSignal(_this.ports.out[0].id, sum);
    });
    return _this;
  }

  _createClass(Back, [{
    key: 'updateConfig',
    value: function updateConfig(config) {
      var _this2 = this;

      // config event


      var portsToRemove = _.clone(this.ports);
      _.each(config.ports, function (port) {
        var oldPort = _.find(_this2.ports, { id: port.id });

        if (oldPort) {
          portsToRemove = _.reject(portsToRemove, { id: port.id });
          //edit post
          oldPort = _.assign(oldPort, port);
        } else {
          _this2.ports.push(port);
        }
        //update or add port
      });

      _.each(portsToRemove, function (port) {
        //remove port
        _this2.ports = _.reject(_this2.ports, { id: port.id });
      });
      _get(Back.prototype.__proto__ || Object.getPrototypeOf(Back.prototype), 'updateConfig', this).call(this, config);
    }
  }, {
    key: 'beforeDelete',
    value: function beforeDelete() {
      // fired before delete
    }
  }]);

  return Back;
}(_cpsModule2.default);

exports.default = Back;
//# sourceMappingURL=server-model.js.map