'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  type: 'Add',
  extras: {
    inputCounts: {
      type: 'double',
      label: 'Input count',
      isExternal: true,
      default: 2,
      min: 2
    }
  },
  ports: {
    in: [],
    out: []
  }
};
//# sourceMappingURL=config.js.map