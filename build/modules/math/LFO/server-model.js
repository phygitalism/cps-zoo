'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _cpsModule = require('cps-module');

var _cpsModule2 = _interopRequireDefault(_cpsModule);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Back = function (_CpsModule) {
  _inherits(Back, _CpsModule);

  function Back(id, transport, config) {
    _classCallCheck(this, Back);

    var _this = _possibleConstructorReturn(this, (Back.__proto__ || Object.getPrototypeOf(Back)).call(this, id, transport, config));

    _this.runRoutine();

    // this.onSignal((channel, message) => {
    // });
    return _this;
  }

  _createClass(Back, [{
    key: 'runRoutine',
    value: function runRoutine() {
      var _this2 = this;

      //функция выходного сигнала
      var func = void 0;
      //момент времени, сначала 0
      var t = 0;
      //угловая частота
      var omega = Math.PI * 2 * this.extras.signalFreq.value;
      //амплитуда сигнала
      var amplitude = this.extras.signalAmplitude.value;
      //если в настройках тип сигнала sin
      if (this.extras.signalType.value == 'sin') func = Math.sin;else func = Math.cos;

      var timeout = 1000 / this.extras.fps.value;
      this.interval = setInterval(function () {
        //расчет выходного значения по формуле
        // U = A * sin(omega * t)
        var outValue = amplitude * func(omega * t);
        _this2.emitSignal(_this2.ports.out[0].id, outValue);
        t += 1 / _this2.extras.fps.value; //if fps = 60
      }, timeout);
    }
  }, {
    key: 'stopRoutine',
    value: function stopRoutine() {
      if (this.interval) {
        clearInterval(this.interval);
      }
    }
  }, {
    key: 'updateConfig',
    value: function updateConfig(config) {
      // config event
      _get(Back.prototype.__proto__ || Object.getPrototypeOf(Back.prototype), 'updateConfig', this).call(this, config);
      this.stopRoutine();
      this.runRoutine();
    }
  }, {
    key: 'beforeDelete',
    value: function beforeDelete() {
      this.stopRoutine();
      // fired before delete
    }
  }]);

  return Back;
}(_cpsModule2.default);

exports.default = Back;
//# sourceMappingURL=server-model.js.map