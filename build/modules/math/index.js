'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _add = require('./add');

var _add2 = _interopRequireDefault(_add);

var _LFO = require('./LFO');

var _LFO2 = _interopRequireDefault(_LFO);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  Add: _add2.default,
  Lfo: _LFO2.default
};
//# sourceMappingURL=index.js.map