'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultTypesValues = exports.PortTypedFactory = exports.zooflat = undefined;

var _delays = require('./modules/delays');

var _delays2 = _interopRequireDefault(_delays);

var _test = require('./modules/test');

var _test2 = _interopRequireDefault(_test);

var _const = require('./modules/const');

var _const2 = _interopRequireDefault(_const);

var _renders = require('./modules/renders');

var _renders2 = _interopRequireDefault(_renders);

var _math = require('./modules/math');

var _math2 = _interopRequireDefault(_math);

var _SWD = require('./modules/SWD');

var _SWD2 = _interopRequireDefault(_SWD);

var _portTypedModel = require('./ports/portTypedModel');

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import devices from './modules/devices'

// import basic from './modules/basic'
var zoo = [
// {
//   label:'Basics',
//   items: basic
// },
{
  label: 'Delays',
  items: _delays2.default
}, {
  label: 'Math',
  items: _math2.default
}, {
  label: 'Test',
  items: _test2.default
}, {
  label: 'Constants',
  items: _const2.default
}, {
  label: 'Renders',
  items: _renders2.default
}, {
  label: 'Devices',
  items: _SWD2.default
}];

// basic,, devices, test
// ,test, renders, devices
// _.each([delays, test, consts, renders], (folder) => {
//   _.each(folder, (module, moduleName) => {
//     zoo[moduleName] = module;
//
//   })
// });

exports.default = zoo;
var zooflat = exports.zooflat = _.chain(zoo).transform(function (total, group) {
  _.each(group.items, function (item, key) {
    total[key] = item;
  });
  return total;
}, {}).value();
var PortTypedFactory = exports.PortTypedFactory = _portTypedModel.PortTypedFactory;
var defaultTypesValues = exports.defaultTypesValues = _portTypedModel.defaultTypesValues;
// {
//   basic: basic,
//   delays: delays,
//   test: test,
//   devices: devices
// }
//# sourceMappingURL=index.js.map