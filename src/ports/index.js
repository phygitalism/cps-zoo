import {BooleanPortModel} from './boolean'
import {NumberPortModel} from './number'

export default {
  boolean: BooleanPortModel,
  double: NumberPortModel
}