import * as SRD from "storm-react-diagrams";
import * as _ from "lodash";
import {PortTypedModel} from './portTypedModel'

export class BooleanPortModel extends PortTypedModel{
	

	
	constructor(type, isInput, name, label = 'port'){
		super(type, isInput, name, label);
	}
	
}

export class BooleanPortFactory extends SRD.AbstractInstanceFactory{

	constructor(){
		super("BooleanPortModel");
	}

	getInstance(){
		return new BooleanPortModel();
	}
}