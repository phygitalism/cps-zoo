import * as SRD from "storm-react-diagrams";
import * as _ from "lodash";
import * as React from "react";

const typesSigns = {
  'boolean':'B',
  'double':'D',
  'int':'I',
  'string':'S'
}

export const defaultTypesValues = {
  'boolean':false,
  'double':0,
  'int':0,
  'string':''
}


export class PortTypedModel extends SRD.DefaultPortModel{



  constructor(type, isInput, name, label = 'port', id){
    super(isInput, name, label, id);
    this.type = type
  }
  //
  serialize(){
  	return _.merge(super.serialize(),{
  		type: this.type,
  	});
  }

  deSerialize(data){
  	super.deSerialize(data);
  	this.type = data.type;
  }

}

export class TypedPortWidget extends SRD.PortWidget{

  constructor(props){
    super(props);
    this.state = {
      selected: false,
    };
  }

  render() {
    let portClass = 'port ';

    if(this.state.selected){
      portClass+='selected '
    }
    return (
      React.DOM.div({
        onMouseEnter: () =>{
          this.setState({selected: true});
        },
        onMouseLeave: () => {
          this.setState({selected: false});
        },
        key:this.props.id,
        className:portClass,
        'data-name':this.props.model.name,
        'data-nodeid': this.props.node.getID()
      })
    );
  }
}

export class TypedPortLabel extends React.Component{
  render() {
    let port = React.createElement(TypedPortWidget, {model:this.props.model, name: this.props.model.name, node: this.props.model.getParent()});
    let label = React.DOM.div({className: 'name'}, [
      React.DOM.div({
        className:'label name',
        key:'a'
      },this.props.model.label ),
      React.DOM.div({
        className:'typelabel name',
        key:'b'
      },' ['+typesSigns[this.props.model.type]+']')
    ]);

    return React.DOM.div({className: (this.props.model.in ? 'in' : 'out') + '-port'},
      this.props.model.in ? port : label,
      this.props.model.in ? label : port,
    );
  }
}

export class PortTypedFactory extends SRD.AbstractInstanceFactory{

  constructor(){
    super("PortTypedModel");
  }

  getInstance(){
    return new PortTypedModel();
  }
}