
import * as SRD from "storm-react-diagrams";
import * as _ from "lodash";

export class NumberPortModel extends SRD.DefaultPortModel{



  constructor(type, isInput, name, label = 'port'){
    super(type, isInput, name, label);
  }

	// serialize(){
	// 	return _.merge(super.serialize(),{
	// 		position: this.position,
	// 	});
	// }
  //
	// deSerialize(data){
	// 	super.deSerialize(data);
	// 	this.position = data.position;
	// }

}

export class NumberPortFactory extends SRD.AbstractInstanceFactory{

	constructor(){
		super("NumberPortModel");
	}

	getInstance(){
		return new NumberPortModel();
	}
}