import CpsModule from 'cps-module'

export default class Back extends CpsModule{
  constructor(id, transport, config){
    super(id, transport, config);
    this.interval = null;
    this.turnOn();
  }

  turnOn(){
    if(this.interval){
      clearTimeout(this.interval)
    }
    this.emitSignal(this.ports.out[0].id, true);
    this.interval = setTimeout(this.turnOff.bind(this), this.params.periodOn.value);
  }

  turnOff(){
    if(this.interval){
      clearTimeout(this.interval)
    }
    this.emitSignal(this.ports.out[0].id, false);
    this.interval = setTimeout(this.turnOn.bind(this), this.params.periodOff.value);
  }

  updateConfig(config) {
    if(this.interval){
      clearTimeout(this.interval)
    }
    this.turnOn();
    super.updateConfig(config);
  }

  beforeDelete(){
    if(this.interval){
      clearTimeout(this.interval)
    }
  }
}