export default {
    type: 'pinger',
    extras:{
      periodOn:{
        type: 'number',
        label:'on period',
        isExternal:true,
        default: 2000,
        min: 10
      },
      periodOff:{
        type: 'number',
        label:'off period',
        isExternal:true,
        default: 500,
        min: 10
      },
    },
    ports: {
      out: [
        {
          type: 'boolean',
          name:'out0',
          label: 'out',
          desc: 'Changes state after period'
        }
      ]
    }
}