import ServerModel from './server-model'
import config from './config'
import {DebugDisplayNodeFactory} from './storm/InstanceFactories'
import {DebugDisplayNodeModel} from './storm/NodeModel'
import {DebugDisplayNodeWidgetFactory} from './storm/NodeWidget'
import {WidgetFactory} from './storm/WidgetFactory'

export default {
  config: config,
  _class:'DebugDisplayNodeModel',
  ServerModel: ServerModel,
  NodeFactory: DebugDisplayNodeFactory,
  NodeModel: DebugDisplayNodeModel,
  NodeWidget: DebugDisplayNodeWidgetFactory,
  WidgetFactory: WidgetFactory
}