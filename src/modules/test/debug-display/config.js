
export default {
  type: 'debug-display',
  extras:{},
  ports: {
    in: [
      {
        type: 'double',
        name: 'in0',
        label: 'in',
        desc: 'Number in'
      },
      {
        type: 'int',
        name: 'in1',
        label: 'in',
        desc: 'Number in'
      },
      {
        type: 'string',
        name: 'in2',
        label: 'in',
        desc: 'Number in'
      }

    ]
  }
}


