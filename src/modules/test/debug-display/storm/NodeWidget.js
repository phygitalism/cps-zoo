import * as React from "react";
import * as SRD from "storm-react-diagrams";
import CONST from '../../../../const'
import config from '../config'
import * as _ from "lodash";
let  div = React.DOM.div;
import {TypedPortWidget,TypedPortLabel} from '../../../../ports/portTypedModel'


/**
 * @author Dylan Vorster
 */
export class DebugDisplayNodeWidget extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
		}
	}


	render() {
    let ledClass= 'led-widget ';

    return (
      React.DOM.div({className: 'basic-node '+config.type+'-node', style: {position: 'relative', width: this.props.size,
          background: this.props.node.color || CONST.defaultBGColor,
          height: this.props.size}},
        div({className:'title'},
          div({className:'name'},this.props.node.name),
          div({className: 'activity ' + (this.props.node.activitySignal?'active':'')},'')
        ),
				div({className:'display-label'}, Math.round(this.props.node.valueToDisplay*1000)/1000),
        div({className:'ports'},
          div({className: 'in'}, _.map(this.props.node.getInPorts(),(port) => {
            return React.createElement(TypedPortLabel,{model:port, node: this.props.node, key:port.id});
          })),
          div({className: 'out'}, _.map(this.props.node.getOutPorts(),(port) => {
            return React.createElement(TypedPortLabel,{model:port, node: this.props.node, key: port.id});
          })),
        )
      )
    )
	}
}

export const DebugDisplayNodeWidgetFactory = React.createFactory(DebugDisplayNodeWidget);

