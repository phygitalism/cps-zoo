import * as SRD from "storm-react-diagrams";
import {DebugDisplayNodeModel} from "./NodeModel";

export class DebugDisplayNodeFactory extends SRD.AbstractInstanceFactory{
	
	constructor(){
		super("DebugDisplayNodeModel");
	}
	
	getInstance(initConfiguration){
		console.log('DebugDisplayNodeFactory ',initConfiguration)
		return new DebugDisplayNodeModel(initConfiguration);
	}
}

