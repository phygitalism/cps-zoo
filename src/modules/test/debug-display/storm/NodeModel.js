import * as SRD from "storm-react-diagrams";
import PortModels from "../../../../ports";
import * as _ from 'lodash'
import {PortTypedModel} from '../../../../ports/portTypedModel'
import {TypedNodeModel} from '../../../../storm-defaults/NodeModel'

export class DebugDisplayNodeModel extends TypedNodeModel{

  constructor(config){
    super(config);
    this.valueToDisplay = '-';
  }

  setData(msg){
    console.log('display -... ', msg);
    this.valueToDisplay = msg;
  }

}