import CONFIG from '../config.js'
import * as SRD from "storm-react-diagrams";
import {DebugLedNodeWidgetFactory} from "./NodeWidget";
import * as React from "react";

export class WidgetFactory extends SRD.NodeWidgetFactory{
	
	constructor(){
		super(CONFIG.type);
	}
	
	generateReactWidget(diagramEngine,node){
    return React.createElement(DebugLedNodeWidgetFactory,{node: node,diagramEngine: diagramEngine});
	}
}