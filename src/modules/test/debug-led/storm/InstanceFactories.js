import * as SRD from "storm-react-diagrams";
import {DebugLedNodeModel} from "./NodeModel";

export class DebugLedNodeFactory extends SRD.AbstractInstanceFactory{
	
	constructor(){
		super("DebugLedNodeModel");
	}
	
	getInstance(initConfiguration){
		return new DebugLedNodeModel(initConfiguration);
	}
}

