export default class SDS
{
    constructor()
	{
		//массив найденых устройств в сети 
		this._availableDevices = []; 
	}
	//метод без реализации определен только для расширения
	//если метод запущен то в this._availableDevices добавляются устройства которые откликнулись  
	startSearching()
	{ }
	//метод без реализации определен только для расширения 
	stopSearching()
	{
		this._availableDevices = []; 
	 }
	//метод с реализацией возращает список доступных устройств 
	getAvailableDevices()
	{
		return this._availableDevices; 
	}
	//метод удаляющий устройство из списка доступных 
	removeAvailable(device)
	{
		//ищем индекс элемента с хостом host 
		var _index = this._availableDevices.indexOf(device); 
		this._availableDevices.splice(_index, 1); 
	}
}