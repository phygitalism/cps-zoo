import Device from '../common/abstractDevice';

var http = require('http'); 

export default class CPS_Led extends Device
{
    constructor(sds, host, config)
    {
        super(sds, host);
        this.config = config;
    }
    //начало работы с лампочкой 
    run()
    {
        this._sds.sendRunToLed(this); 
        super.run(); 
    }
    //зажечь первую лампочку 
    firstLedOn()
    {
        var command = 'http://' + this.getHost() + this.config.FirstLedOn.value ;
        http.get(command).on('error', (err)=> { }); 
    }
    //погасить первую лампочку 
    firstLedOff()
    {
        var command = 'http://'+ this.getHost() + this.config.FirstLedOff.value;
        http.get(command).on('error', (err)=> {}); 
    }
    //прекратить работу с лампчкой  
    stop()
    {
        var command = 'http://' + this.getHost() + this.config.Stop.value;
        http.get(command).on('error', (err) => {}); 
        super.stop(); 
        // this = null ;
    }
    
}