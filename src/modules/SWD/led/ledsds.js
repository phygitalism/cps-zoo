import SDS from '../common/abstractSDS.js';
import LedDevice from './leddevice'
var dgram = require('dgram'); 


export default class SDS_CPS_Led extends SDS
{
    constructor(config)
    {
        super();
        this.config = config;
        console.log(' >>> ', config);
        this.udp_Socket = dgram.createSocket('udp4');
    }
    //начать поиск лампочек в сети 
    startSearching()
    {
        // setInterval(() => {
        //     console.log(' .. sending to ',this.config.MSG_HI_TO_LED.value,
        //       this.config.UDP_PORT.value,
        //       this.config.BROADCAST_IP.value,":", this.config.BROADCAST_FREQ.value)
        //     this.udp_Socket.send(this.config.MSG_HI_TO_LED.value,
        //       this.config.UDP_PORT.value,
        //       this.config.BROADCAST_IP.value);
        // }, this.config.BROADCAST_FREQ.value);
        //
        // this.udp_Socket.on('message', (msg, rinfo)=> {
        //     if (msg.toString() == this.config.MSG_HI_FROM_LED.value)
        //     {
        //         for (var i = 0; i < this._availableDevices.length; i ++ )
        //         {
        //           if (this._availableDevices[i] == rinfo.address)
        //             return;
        //             if (this._availableDevices[i].getHost() == rinfo.address)
        //                 return;
        //         }
        //
        //         var led = new LedDevice(this, rinfo.address, this.config);
        //         this._availableDevices.push(led);
        //     }
        // });
      var led = new LedDevice(this, '192.168.88.109', this.config);
      this._availableDevices.push(led);
    }
    //остановить поиск лампочек 
    stopSearching()
    {
        this.udp_Socket.close(); 
        super.stopSearching(); 
    }
    
    sendRunToLed(led)
    {
        this.udp_Socket.send('run', this.config.UDP_PORT.value,led.getHost());
    }
}