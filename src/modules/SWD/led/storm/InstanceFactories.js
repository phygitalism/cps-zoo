import * as SRD from "storm-react-diagrams";
import {LedNodeModel} from "./NodeModel";

export class LedNodeFactory extends SRD.AbstractInstanceFactory{
	
	constructor(){
		super("LedNodeModel");
	}
	
	getInstance(){
		return new LedNodeModel();
	}
}

