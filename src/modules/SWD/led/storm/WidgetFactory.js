import CONFIG from '../config.js'
import * as SRD from "storm-react-diagrams";
import {LedNodeWidgetFactory} from "./NodeWidget";
import * as React from "react";

export class LedWidgetFactory extends SRD.NodeWidgetFactory{
	
	constructor(){
		super(CONFIG.type);
	}
	
	generateReactWidget(diagramEngine,node){
		return React.createElement(LedNodeWidgetFactory,{node: node,diagramEngine: diagramEngine});
	}
}