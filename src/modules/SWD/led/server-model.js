import CpsModule from 'cps-module'
import LedSDS from './ledsds'
import LedDevice from './leddevice'

// 1) экзем LedSDS
// 2) startSearching (вызвать сразу)
// 3) getAvailable devices
// 4) device.getHost
// 5) device.run


export default class Back extends CpsModule{
  constructor(id, transport, config){

    super(id, transport, config);
    this.device = {};
    let sds = new LedSDS(this.params);
    sds.startSearching();
    let interval = setInterval(()=>{
      let devicesList = sds.getAvailableDevices();
      this.device = devicesList[0];
      console.log('+ device', this.device);
      if(this.device){
        this.device.run();
        clearInterval(interval)
      }
    }, 3000);

    this.onSignal((channel, message) => {
      if(this.device){
        if(message){
          this.device.firstLedOn()
        }else{
          this.device.firstLedOff()
        }
      }
    });
  }
}