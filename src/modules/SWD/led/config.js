export default {
    type: 'leddevice',

    extras:{
        PORT: {
            type: 'port',
            isExternal:true,
            default: 6002,
            required: true
        },
      MSG_HI_TO_LED: {
        type: 'string',
        isExternal:true,
        default: 'hey',
        required: true
      },
      MSG_HI_FROM_LED:{
        type: 'string',
        isExternal:true,
        default: 'Led',
        required: true
      },
      BROADCAST_IP:{
        type: 'string',
        isExternal: true,
        default: '192.168.88.126',
        required: true
        // regex: 'http\:\/\/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
      },
      UDP_PORT:{
        type: 'int',
        isExternal:true,
        default: 5002,
        required: true
      },
      BROADCAST_FREQ:{
        type: 'double',
        isExternal:true,
        default: 1000,
        required: true
      },
      "FirstLedOn": {
        type: 'string',
        default: '/Led/On/firstled',
        required: true
      },
      "FirstLedOff": {
        type: 'string',
        default: '/Led/Off/firstled',
        required: true
      },
      "Stop": {
        type: 'string',
        default: '/StartUDP',
        required: true
      }
    },
    ports: {
        in: [
          {
            type: 'boolean',
            name: 'ledswitch',
            label: 'in',
            desc: 'In port'
          }
        ]
    }
}
