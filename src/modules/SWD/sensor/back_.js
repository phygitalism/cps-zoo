import Device from '../../AbstractDevice/back.js'; 

var config = require('./config.json'); 
var http = require('http'); 

export default class CPS_LightSensor extends Device
{
    constructor(sds, host)
    {
        super(sds, host); 
    }
    //начать работу с датчиком света 
    run()
    {
        this._sds.sendRunToSensor(this); 
        super.run(); 
    }
    //получить уровень освещенности 
    getLightValue()
    {
        var command = 'http://' + this.getHost() + config.LIGHT_URI; 
        
        let strData = ''; 

        http.get(command, (res)=> {
            
            res.on('data', (chunk) => {
                strData += chunk; 
            }); 

            res.on('end', () => {
                try
                {
                    return parseInt(strData);
                }
                catch(e)
                {
                    console.log(e.message); 
                }                 
            });
        }).on('error', () => {});
    }
    //закончить работу с датчиком света 
    stop()
    {
        var command = 'http://' + this.getHost() + config.STOP; 
        http.get(command).on('error', (err) => {}); 
        super.stop(); 
        // this = null ;
    }
    

        
}