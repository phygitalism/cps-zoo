import SDS from '../../AbctractSDS/back.js'; 
import CPS_LightSensor from '../../../Devices/CPS_Devices/CPS_LightSensor/back.js';

var config = require('./config.json'); 
var dgram = require('dgram');

export default class SDS_CPS_LightSensor extends SDS
{
    constructor()
    {
        super();   
        this.udp_Socket = dgram.createSocket('udp4'); 
    }
    //начать поиск датчиков в сети 
    startSearching()
    {
        setInterval(() => {
            this.udp_Socket.send(config.MSG_HI_TO_SENSOR, config.UDP_PORT, config.BROADCAST_IP); 
        }, config.BROADCAST_FREQ);

        this.udp_Socket.on('message', (msg, rinfo)=> {
            if (msg.toString() == config.MSG_HI_FROM_SENSOR)
            {
                for (var i = 0; i < this._availableDevices.length; i ++ )
                {
                    if (this._availableDevices[i].getHost() == rinfo.address)
                        return; 
                }

                var sensor = new CPS_LightSensor(this, rinfo.address); 
                this._availableDevices.push(sensor); 
            }
        }); 
    }
    //остановить поиск лампочек 
    stopSearching()
	{ 
        this.udp_Socket.close(); 
        super.stopSearching(); 
    }

    sendRunToSensor(sensor)
    {
        this.udp_Socket.send(config.MSG_RUN, config.UDP_PORT, sensor.getHost()); 
    }
    
}