

export default {
    type: 'unity',
    extras:{
      PORT: {
        type: 'port',
        isExternal:true,
        default: 6002,
        required: true
      }
    },
    ports: {
      in: [

      ],
      out: [

      ]
    }
}