import ServerModel from './server-model'
import config from './config'
import {TypedNodeFactory} from '../../../storm-defaults/InstanceFactories'
import {TypedNodeModel} from '../../../storm-defaults/NodeModel'
import {TypedNodeWidget} from '../../../storm-defaults/NodeWidget'
import {WidgetFactory} from './storm/WidgetFactory'

export default {
  config: config,
  ServerModel: ServerModel,
  _class:'TypedNodeModel',
  NodeFactory: TypedNodeFactory,
  NodeModel: TypedNodeModel,
  NodeWidget: TypedNodeWidget,
  WidgetFactory: WidgetFactory
}