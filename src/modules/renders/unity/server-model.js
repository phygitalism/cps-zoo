import CpsModule from 'cps-module'
// import config from './config'

let server = require('http').createServer();
let io = require('socket.io')(server);
let moduleAlreadyConfigurated = false;


export default class Back extends CpsModule{
  constructor(id, transport, config){

    super(id, transport, config);
    this.sockets = [];

    io.on('connection', (socket) => {
      this.sockets.push(socket);
      console.log('\n\n!!! \nuser connected');

      socket.on('signalOut',(msg)=>{
        console.log('\n\nsignalOut >> ',msg);
        try {
          if (typeof msg == 'string') {
            msg = JSON.parse(msg)
          }
        }catch(e) {
          console.log('err', e);
        }
        this.emitSignal(msg.portId, msg.data);
      });

      socket.on('initConfiguration',(msg)=>{
        console.log('\n\n initConfiguration >> ',typeof msg, msg);
        if(moduleAlreadyConfigurated){
          return;
        }
        try {
          if (typeof msg == 'string') {
            msg = JSON.parse(msg)
          }
        }catch(e) {
            console.log('err', e);
        }
        console.log('2 initConfiguration >> ',typeof msg);
        moduleAlreadyConfigurated = true;
        this.updateConfig(msg);
      });

    });
    let ii=true;

    this.onSignal((channel, message) => {
      console.log(' >>> SIGNAL IN', channel, message)
      io.emit('signalIn',{portId: channel, data:message})
    });

    console.log('UNITY STARTED', this.params.PORT.value )
    server.listen(this.params.PORT.value || '6002');
  }
}