import CONFIG from '../config.js'
import * as SRD from "storm-react-diagrams";
import {TypedNodeWidgetFactory} from "../../../../storm-defaults/NodeWidget";
import * as React from "react";

export class WidgetFactory extends SRD.NodeWidgetFactory{
	
	constructor(){
		super(CONFIG.type);
	}
	
	generateReactWidget(diagramEngine,node){
		return React.createElement(TypedNodeWidgetFactory,{node: node,diagramEngine: diagramEngine});
	}
}