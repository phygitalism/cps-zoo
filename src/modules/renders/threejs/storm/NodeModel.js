import {TypedNodeModel} from '../../../../storm-defaults/NodeModel'

export class DebugLedNodeModel extends TypedNodeModel{

  constructor(config){
    super(config);
  }

  setData(msg){
    this.data.on = String(msg)==String(true)
  }

}