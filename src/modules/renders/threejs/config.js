
export default {
  type: 'debug-led',
  extras:{},
  ports: {
    in: [
      {
        type: 'boolean',
        name: 'in0',
        label: 'in',
        desc: 'Led in'
      }
    ]
  }
}


