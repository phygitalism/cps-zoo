import ServerModel from './server-model'
import config from './config'
import {DebugLedNodeFactory} from './storm/InstanceFactories'
import {DebugLedNodeModel} from './storm/NodeModel'
import {DebugLedNodeWidgetFactory} from './storm/NodeWidget'
import {WidgetFactory} from './storm/WidgetFactory'

export default {
  config: config,
  _class:'DebugLedNodeModel',
  ServerModel: ServerModel,
  NodeFactory: DebugLedNodeFactory,
  NodeModel: DebugLedNodeModel,
  NodeWidget: DebugLedNodeWidgetFactory,
  WidgetFactory: WidgetFactory
}