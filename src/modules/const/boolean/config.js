export default {
    type: 'const_boolean',
    extras:{
      state:{
        type: 'boolean',
        label:'state',
        isExternal:true,
        default: false
      }
    },
    ports: {
      out: [
        {
          type: 'boolean',
          name:'out0',
          label: 'out',
          desc: 'Boolean out'
        }
      ]
    }
}