import ConstBoolean from './boolean'
import ConstNumber from './number'

export default {
  ConstBoolean: ConstBoolean,
  ConstNumber: ConstNumber
}