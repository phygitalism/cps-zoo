import CpsModule from 'cps-module'

export default class Back extends CpsModule{
  constructor(id, transport, config){
    super(id, transport, config);
  }


  updateConfig(config) {

    if(config && config.extras && config.extras.state && this.extras.state.value!=config.extras.state) {
      this.emitSignal(this.ports.out[0].id, config.extras.state.value);
      this.ports.out[0].value = config.extras.state.value;
    }
    super.updateConfig(config);
  }
}