export default {
    type: 'const_number',
    extras:{
      state:{
        type: 'double',
        label:'state',
        isExternal:true,
        default: 100
      }
    },
    ports: {
      out: [
        {
          type: 'double',
          name:'out0',
          label: 'out',
          desc: 'Number out'
        }
      ]
    }
}