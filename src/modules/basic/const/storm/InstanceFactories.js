import * as SRD from "storm-react-diagrams";
import {ConstNodeModel} from "./NodeModel";

export class ConstNodeFactory extends SRD.AbstractInstanceFactory{
	
	constructor(){
		super("ConstNodeModel");
	}
	
	getInstance(){
		return new ConstNodeModel();
	}
}

