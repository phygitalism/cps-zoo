import CONFIG from '../config.js'
import * as SRD from "storm-react-diagrams";
import {ConstNodeWidgetFactory} from "./NodeWidget";

export class ConstWidgetFactory extends SRD.NodeWidgetFactory{
	
	constructor(){
		super(CONFIG.type);
	}
	
	generateReactWidget(diagramEngine,node){
		return ConstNodeWidgetFactory({node: node});
	}
}