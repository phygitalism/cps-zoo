import { TYPES } from 'cps-module'
import ports from '../../../ports/index'

export default {
  type: 'basic/const',
  extra:{
    isFloat:{
      type: TYPES.params.boolean,
      isExternal:true,
      default: false
    }
  },
  ports: {
    out: [
      {
        type: ports.number,
        id: 'out0',
        name: 'out',
        desc: 'Number'
      }
    ]
  }
}
