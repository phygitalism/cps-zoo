import CpsModule from 'cps-module'
import config from './config'

export default class extends CpsModule{
  constructor(id, transport, params){
    super(id, transport, config, params);

  }
}