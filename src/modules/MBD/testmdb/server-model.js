import CpsModule from 'cps-module'

export default class Back extends CpsModule{
  constructor(id, transport, config){
    super(id, transport, config);

    this.onSignal((portId, message) => {
    // do something here
      this.emitSignal(this.ports.out[0].id, {message:'some'});
    });
  }


  updateConfig(config) {
    // config event
    super.updateConfig(config);

    //Update only if extra changes and value is different
    // if(config && config.extras && config.extras.state && this.extras.state.value!=config.extras.state) {
    //   this.emitSignal(this.ports.out[0].id, this.extras.state.value);
    // }
  }

  beforeDelete(){
    // fired before delete
  }
}