export default [
  {
    label:'le matrix',
    id:'matrix',
    binPath: ".\\bin\\RandomMatrix"

  }, {
    label: 'le sinus',
    id: 'sinus',
    binPath: ".\\bin\\Sinus"
  }
]