export default {
    type: 'template',
    extras:{
      model:{
        type: 'string',
        label:'model type',
        isExternal:true,
        default: 'sinus.cs'
      }
    },
    ports: {
      in: [

      ],
      out: [

      ]
    }
}