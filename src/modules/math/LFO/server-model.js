import CpsModule from 'cps-module'


export default class Back extends CpsModule{
  constructor(id, transport, config){

    super(id, transport, config);

    this.runRoutine()

    // this.onSignal((channel, message) => {
    // });
  }

  runRoutine(){
    //функция выходного сигнала
    let func;
    //момент времени, сначала 0
    let t = 0 ;
    //угловая частота
    let omega = Math.PI * 2 * this.extras.signalFreq.value;
    //амплитуда сигнала
    let amplitude = this.extras.signalAmplitude.value;
    //если в настройках тип сигнала sin
    if (this.extras.signalType.value == 'sin')
      func = Math.sin;
    else
      func = Math.cos;

    let timeout = 1000/this.extras.fps.value;
    this.interval = setInterval(()=>{
      //расчет выходного значения по формуле
      // U = A * sin(omega * t)
      let outValue = amplitude * func(omega * t);
      this.emitSignal(this.ports.out[0].id, outValue);
      t += 1/this.extras.fps.value;    //if fps = 60
    }, timeout);
  }

  stopRoutine(){
    if(this.interval){
      clearInterval(this.interval);
    }
  }

  updateConfig(config) {
    // config event
    super.updateConfig(config);
    this.stopRoutine();
    this.runRoutine();

  }

  beforeDelete(){
    this.stopRoutine()
    // fired before delete
  }
}