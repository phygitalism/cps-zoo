
export default {
    type: 'Lfo',
    extras:{
      signalType:{
        type: 'string',
        label:'Signal type',
        isExternal:true,
        enum:['sin','sqr', 'tri'],
        default: 'sin'
      },
      fps:{
        type: 'double',
        label:'FPS',
        isExternal:true,
        min:0,
        default: 1
      },
      signalFreq:{
        type: 'double',
        label:'Signal freq',
        isExternal:true,
        min:0,
        default: 10
      },
      signalAmplitude:{
        label:'Signal amp',
        type: 'double',
        isExternal:true,
        min:0,
        default: 1
      }
    },
    ports: {
      out: [
        {
          type: 'double',
          name: 'generatorout',
          label: 'out',
          desc: 'Out port'
        }
      ]
    }
}