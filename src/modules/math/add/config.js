export default {
    type: 'Add',
    extras:{
      inputCounts:{
        type: 'double',
        label:'Input count',
        isExternal:true,
        default: 2,
        min: 2
      }
    },
    ports: {
      in:[

      ],
      out: [

      ]
    }
}