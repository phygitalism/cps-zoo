import CpsModule from 'cps-module'
import * as _ from 'lodash'

export default class Back extends CpsModule{
  constructor(id, transport, config){
    super(id, transport, config);

    this.updateConfig({ports:[
        {
          type: 'double',
          name:'in0',
          id:'in0',
          label: 'in 0',
          in:true,
          desc: ''
        },
        {
          type: 'double',
          name:'in1',
          id:'in1',
          label: 'in 1',
          in:true,
          desc: ''
        },
        {
          type: 'double',
          name:'out0',
          id:'out0',
          label: 'out',
          in:false,
          desc: 'Sum of inputs'
        }
      ]
    });
    this.onSignal((portId, message) => {
    // do something here
      let sum = _.reduce(this.ports.in,(total, port)=>{
        console.log('add port ', port);
        return total + (port.value || 0);
      },0);

      this.emitSignal(this.ports.out[0].id, sum);
    });
  }


  updateConfig(config) {
    // config event


    let portsToRemove = _.clone(this.ports)
    _.each(config.ports, (port)=>{
      let oldPort = _.find(this.ports,{id:port.id});

      if(oldPort){
        portsToRemove = _.reject(portsToRemove, {id: port.id});
        //edit post
        oldPort = _.assign(oldPort, port);
      }else{
        this.ports.push(port)
      }
      //update or add port
    });

    _.each(portsToRemove,(port)=>{
      //remove port
      this.ports = _.reject(this.ports, {id:port.id})
    });
    super.updateConfig(config);
  }

  beforeDelete(){
    // fired before delete
  }
}