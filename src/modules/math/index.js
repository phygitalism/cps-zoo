import Add from './add'
import LFO from './LFO'

export default {
  Add: Add,
  Lfo: LFO
}