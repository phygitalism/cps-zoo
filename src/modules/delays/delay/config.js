
export default {
    type: 'delay',
    extras:{
      delay:{
        type: 'number',
        label:'delay ms',
        isExternal:true,
        default: 400,
        min: 10
      },
    },
    ports: {
      in: [
        {
          type: 'boolean',
          name: 'delayin',
          label: 'in',
          desc: 'In port'
        },
        {
          type: 'number',
          name: 'indelay',
          label: 'delay ms',
          desc: 'Delay in ms'
        }
      ],
      out: [
        {
          type: 'boolean',
          name: 'delayout',
          label: 'out',
          desc: 'Out port'
        }
      ]
    }
}