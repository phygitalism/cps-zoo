import CpsModule from 'cps-module'

function routine(channel, message){
  if(channel==this.ports.in[1].id){
    return this.updateConfig({extras:{delay:{value:message}}})
  }
  this.interval = setTimeout(()=>{
    this.emitSignal(this.ports.out[0].id, message);
  }, this.params.delay.value);

}

export default class Back extends CpsModule{
  constructor(id, transport, config){
    super(id, transport, config);
    this.bindedRoutine = routine.bind(this);
    this.onSignal(this.bindedRoutine);
  }

  updateConfig(config){
    super.updateConfig(config);
    this.offSignal(this.bindedRoutine)
    this.bindedRoutine = routine.bind(this);
    this.onSignal(this.bindedRoutine);
  }

  beforeDelete(){
    //TODO save delay stack and remove it

    // if(this.interval){
    //   clearTimeout(this.interval)
    // }
  }
}