import ServerModel from './server-model'
import config from './config'
import TestDeviceInstanceFactories from './storm/InstanceFactories'
import TestDeviceNodeModel from './storm/NodeModel'
import TestDeviceNodeWidget from './storm/NodeWidget'
import TestDeviceWidgetFactory from './storm/WidgetFactory'

export default {
  config: config,
  ServerModel: ServerModel,
  InstanceFactories: TestDeviceInstanceFactories,
  NodeModel: TestDeviceNodeModel,
  NodeWidget: TestDeviceNodeWidget,
  WidgetFactory: TestDeviceWidgetFactory
}