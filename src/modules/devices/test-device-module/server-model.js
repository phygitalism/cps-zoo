import CpsModule from 'cps-module'
import config from './config'
import request from 'request'
import express from 'express';
import Promise from 'bluebird'
import dgram from 'dgram';
import debug from 'debug';


const log = debug('driver:test-device');

export default class Back extends CpsModule{
  constructor(id, transport, params){

    super(id, transport, config, params);
    this.app = express();
    this.clients = [];
    this.getPort()
      .then((port) => {
        return this.runHttpServer.bind(this)(port)
      })
      .then(this.sendUDP)
      .catch((err)=>{
        console.error(err);
      });
  }


  runHttpServer(port){
    return new Promise((resolve, reject) => {

      this.app.get('/reg', (req, res) => {
        log('/reg', req.query, req.hostname);
        if(req.query.name == this.options.ext.regToken){
          this.clients.push(req.hostname);
        }
        return res.status(204).send();
      });

      this.app.get('/:cmd', (req, res)=>{
        log('/:cmd', req.query);
        if(req.query.light){
          console.log('light');
          if(req.query.value){
            this.sendAll(req.params.cmd, req.query.value<600 && req.query.value>200);
          }
        }
      });

      this.app.listen(port || this.options.ext.HTTP.PORT);
      log('http server started');
      resolve(null)
    });
  }

  sendAll(cmd, onOff){
    let onOffStr = onOff?'0':'1';
    log('Sending data to MC');
    this.clients.forEach((client)=>{
      request.get('http://'+client+'/?name=1&number=13&pin=Digital&mode=OUT&oldlevel='+onOffStr)
        .on('response', function(response) {
          log('Sending data response', response);
        })
        .on('error', (err)=>{
          log('Sending data error', err);
        })
    })
  }

  startUDPSending(){
    setInterval(()=>{
      udp.send(this.options.ext.UDP.MSG, this.options.ext.UDP.PORT, this.options.ext.UDP.URL,(err, res)=>{
        if(err){
          return log('error: ', err);
        }
        log('response: ',res);
      });
    }, this.options.ext.UDP.BROADCAST_FREQ_MS);
  }

  sendUDP(){
    return new Promise((resolve, reject)=>{
      const udp = dgram.createSocket('udp4');

      this.startUDPSending();

      log('udp broadcast started');
      resolve(null);
    })
  }



}