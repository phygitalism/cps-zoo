import * as SRD from "storm-react-diagrams";
import PortModels from "../../../../ports";
import CONFIG from '../config.js'
import * as _ from 'lodash'

export class TestDeviceDiamondNodeModel extends SRD.NodeModel{
	
	constructor(){
		super(CONFIG.type);
		_.each(CONFIG.ports, (portCfg) => {
			this.addPort(new PortModels[portCfg.type](portCfg.name));
		});
		// this.addPort(new PortModel('top'));
		// this.addPort(new PortModel('left'));
		// this.addPort(new PortModel('bottom'));
		// this.addPort(new PortModel('right'));
	}
	
}