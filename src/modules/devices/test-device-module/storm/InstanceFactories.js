import * as SRD from "storm-react-diagrams";
import {TestDeviceNodeModel} from "./NodeModel";

export class TestDeviceNodeFactory extends SRD.AbstractInstanceFactory{
	
	constructor(){
		super("TestDeviceNodeModel");
	}
	
	getInstance(){
		return new TestDeviceNodeModel();
	}
}

