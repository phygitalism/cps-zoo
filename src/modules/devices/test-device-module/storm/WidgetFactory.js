import CONFIG from '../config.js'
import * as SRD from "storm-react-diagrams";
import {TestDeviceNodeWidgetFactory} from "./NodeWidget";

export class TestDeviceWidgetFactory extends SRD.NodeWidgetFactory{
	
	constructor(){
		super(CONFIG.type);
	}
	
	generateReactWidget(diagramEngine,node){
		return TestDeviceNodeWidgetFactory({node: node});
	}
}