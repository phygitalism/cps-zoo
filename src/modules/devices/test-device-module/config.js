import { TYPES } from 'cps-module'
import ports from '../../../ports/index'
export default {
    type: 'test-device',
    params:{
      UDP: {
        PORT: {
          type: TYPES.params.port,
          isExternal:true,
          default: 5002,
          required: true
        },
        URL: {
          type: TYPES.params.string,
          isExternal:true,
          default: 'http://192.168.88.50',
          required: true,
          regex: 'http\:\/\/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
        },
        MSG: {
          type: TYPES.params.string,
          isExternal:true,
          required: true,
        },
        BROADCAST_FREQ_MS: {
          type: TYPES.params.number,
          isExternal:true,
          default: 3000,
          min: 100
        },
      },
      regToken: {
        type:TYPES.params.string,
        isExternal:true,
        required:true
      },
      HTTP:{
        PORT:{type:TYPES.params.number, default:80, min:0}
      },
      HARDWARE_IP:{
        type:TYPES.params.string,
        default:'http://192.168.88.50',
        regex:'http\:\/\/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
      }
    },
    ports: {
      in: [
        {
          type: ports.boolean,
          id: 'pIn',
          name: 'in',
          desc: 'Led control'
        },
        {
          type: ports.number,
          default: 200,
          id: 'tInLow',
          name: 'on threshold',
          desc: 'The lowest threshold to on the led'
        },
        {
          type: ports.number,
          default: 800,
          id: 'tInHigh',
          name: 'off threshold',
          desc: 'The lowest threshold to off the led'
        }
      ],
      out: [
        {
          type: ports.number,
          id: 'pOutLevel',
          name: 'lightness level',
          desc: 'Light level [0-1000]'
        },
        {
          type: ports.boolean,
          id: 'pOutCtrl',
          name: 'Lamp controll',
          desc: 'Light level [0-1000]'
        }
      ]
    }
}