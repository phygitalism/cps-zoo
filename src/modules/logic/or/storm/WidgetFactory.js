import CONFIG from '../config.js'
import * as SRD from "storm-react-diagrams";
import {TypedNodeWidgetFactory} from "../../../../storm-defaults/NodeWidget";
import * as React from "react";

export class ORWidgetFactory extends SRD.NodeWidgetFactory{
	
	constructor(){
		super(CONFIG.type_id, CONFIG);
	}
	
	generateReactWidget(diagramEngine,node){
		return React.createElement(TypedNodeWidgetFactory, {node: node,diagramEngine: diagramEngine, config:CONFIG});
	}
}