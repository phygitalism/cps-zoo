
export default {
    type: 'multiplexer',
    extras:{
    },
    ports: {
      in: [
        {
          type: 'number',
          name: 'mux_adress',
          label: 'adress',
          desc: 'MUX adress'
        },
        {
          type: 'number',
          name: 'in0',
          label: '0',
          desc: 'MUX input 0'
        }, 
        {
          type: 'number',
          name: 'in1',
          label: '1',
          desc: 'MUX input 1'
        },
        {
          type: 'number',
          name: 'in2',
          label: '2',
          desc: 'MUX input 2'
        }, 
        {
          type: 'number',
          name: 'in3',
          label: '3',
          desc: 'MUX input 3'
        }
      ],
      out: [
        {
          type: 'number',
          name: 'muxout',
          label: 'out',
          desc: 'MUX out'
        }
      ]
    }
}