import CpsModule from 'cps-module'


export default class Back extends CpsModule{
  constructor(id, transport, config){

    super(id, transport, config);

    this.onSignal((channel, message) => {
          let adress = this.ports.in[0].value; 
          let outValue = this.ports.in[adress+1].value; 

        this.emitSignal(this.ports.out[0].id, outValue);
    });
  }
}