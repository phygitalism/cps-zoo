import CpsModule from 'cps-module'


export default class Back extends CpsModule{
  constructor(id, transport, config){

    super(id, transport, config);

    //текущее значение счетчика 
    this._count = 0 ; 

    this.onSignal((channel, message) => {
      //пусть счетчик будет реверсивным (складывает и вычетает)
      //если приходит на вход true, то инкремент суммы  
      //иначе декремент 
      if (this.ports.in[0])
        this._count = (this._count + 1) % this.params.counterModule.value; 
      else {
        this._count--; 
        this._count = this._count >= 0 ? this._count : this.params.counterModule.value + this._count; 
      }   

      this.emitSignal(this.ports.out[0].id, this._count);
    });
  }
}