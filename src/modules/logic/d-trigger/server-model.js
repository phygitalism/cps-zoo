import CpsModule from 'cps-module'


export default class Back extends CpsModule{
  constructor(id, transport, config){
    super(id, transport, config);

    this._oldValue; 

    this.onSignal((channel, message) => {
      
      if (channel.id == this.ports.in[0]){
        this._oldValue = this.ports.in[1].value; 
      }             

      this.emitSignal(this.ports.out[0].id, this._oldValue);
    });
  }
}
