
export default {
    type_id: 'DTrigger',
    extras:{
    },
    ports: {
        in: [
            {
                type: 'boolean', 
                name: 'strobe', 
                label: 'strobe', 
                desc: 'Strobe input'
            },
            {
                type: 'number', 
                name: 'in1',
                label: 'in', 
                desc: 'Information input'
            }
        ], 

      out: [
        {
          type: 'number',
          name: 'DTriggerout',
          label: 'out',
          desc: 'Out port'
        }
      ]
    }
}