import * as SRD from "storm-react-diagrams";
import {TypedNodeModel} from "./NodeModel";

export class TypedNodeFactory extends SRD.AbstractInstanceFactory{
	
	constructor(){
		super("TypedNodeModel");
	}
	
	getInstance(initConfiguration){
		console.log('TypedNodeFactory ',initConfiguration)
		return new TypedNodeModel(initConfiguration);
	}
}

