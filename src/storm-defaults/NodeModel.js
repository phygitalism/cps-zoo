import * as SRD from 'storm-react-diagrams'
import * as _ from "lodash";
import {PortTypedModel} from '../ports/portTypedModel'

const NodeModel = SRD.NodeModel;
const AbstractInstanceFactory = SRD.AbstractInstanceFactory;


export class TypedNodeInstanceFactory extends AbstractInstanceFactory {

  constructor(a,b){
    console.log('TypedNodeInstanceFactory++ ', a, b);
    super("TypedNodeModel");
  }

  getInstance(initialConfig){
    console.log('TypedNodeInstanceFactory', initialConfig);
    return new TypedNodeModel(initialConfig);
  }
}

/**
 * @author Dylan Vorster
 */

export class TypedNodeModel extends NodeModel{

  constructor(config){
    super(config.type);
    this.name = config.type;
    this.x = config.x;
    this.y = config.y;
    this.data = {};
    this.id = config.id;
    this.color = config.color || 'rgb(70,70,80)';
    this.activitySignal = false;
    this.activityTimeout;
    this.extras = config.extras;
    this.config = config;
    if(!_.isEmpty(config.ports.in) || !_.isEmpty(config.ports.out)){
      _.map(config.ports.in, (port) => {
        this.addPort(new PortTypedModel(port.type, true, port.name, port.label, port.id));
      });

      _.map(config.ports.out, (port) => {
        this.addPort(new PortTypedModel(port.type, false, port.name, port.label, port.id));
      });
    }else{
      _.map(config.ports, (port) => {
        this.addPort(new PortTypedModel(port.type, port.in, port.name, port.label, port.id));
      });
    }
  }

  setData(obj){
    Object.assign(this.data, obj);
  }
  setActivity(forceUpdate){
    this.activitySignal = true;
    if(forceUpdate){forceUpdate()}
    if(this.activityTimeout){
      clearTimeout(this.activityTimeout)
    }
    this.activityTimeout = setTimeout(()=>{
      this.activitySignal = false;
      if(forceUpdate){forceUpdate()}
    },100)
  }
  deSerialize(object){
    super.deSerialize(object);
    this.name = object.name;
    this.color = object.color;
  }

  serialize(){
    return _.merge(super.serialize(),{
      name: this.name,
      color: this.color,
    });
  }

  getInPorts(){
    return _.filter(this.ports,(portModel) => {
      return portModel.in;
    });
  }

  getOutPorts(){
    return _.filter(this.ports,(portModel) => {
      return !portModel.in;
    });
  }
}