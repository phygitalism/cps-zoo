
// import basic from './modules/basic'
import delays from './modules/delays'
import test from './modules/test'
import consts from './modules/const'
import renders from './modules/renders'

import math from './modules/math'
import SWD from './modules/SWD'
// import devices from './modules/devices'
import {PortTypedFactory as _PortTypedFactory,
  defaultTypesValues as _defaultTypesValues} from './ports/portTypedModel'

import * as _ from 'lodash'

let zoo = [
  // {
  //   label:'Basics',
  //   items: basic
  // },
  {
    label:'Delays',
    items: delays
  },
  {
    label:'Math',
    items: math
  },
  {
    label:'Test',
    items: test
  },
  {
    label:'Constants',
    items: consts
  },
  {
    label:'Renders',
    items: renders
  },
  {
    label: 'Devices',
    items: SWD
  }
];

// basic,, devices, test
// ,test, renders, devices
// _.each([delays, test, consts, renders], (folder) => {
//   _.each(folder, (module, moduleName) => {
//     zoo[moduleName] = module;
//
//   })
// });

export default  zoo;
export const zooflat = _.chain(zoo).transform((total, group)=>{
  _.each(group.items, (item, key)=>{
    total[key]=item;
  });
    return total;
  }, {})
  .value();
export const PortTypedFactory = _PortTypedFactory;
export const defaultTypesValues= _defaultTypesValues;
// {
//   basic: basic,
//   delays: delays,
//   test: test,
//   devices: devices
// }
