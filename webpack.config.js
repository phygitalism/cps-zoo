const path = require('path');
const glob = require('glob');

function toObject(paths) {
  var ret = {};

  paths.forEach(function(path) {
    // you can define entry names mapped to [name] here
    ret[path.split('/').slice(-1)[0]] = path;
  });

  return ret;
}

module.exports = {
  entry: toObject(glob.sync("./src/**/*.js")),
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: "[name]"
  },
  resolve: {
    // Add '.ts' and '.tsx' as a resolvable extension.
    modules: [path.resolve('./src')],
    extensions: [ ".ts", ".tsx", ".js"]
  },
  target: 'node',
  externals: {
    'debug': 'debug',
    'lodash': 'window["lodash"]',
    'fs': 'fs',
    'http':'http',
    'socket.io':'window["socket.io"]',
    'socket.io-client':'window["socket.io-client"]',
    'react': 'react',
    'cps-module': 'window["cps-module"]',
    'express': 'express',
    'request': 'request',
    'storm-react-diagrams':'window["storm-react-diagrams"]',
    'bluebird': 'bluebird'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      }
    ]
    // loaders: [
    //   // all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
    //   { test: /\.tsx?$/, loader: "ts-loader" },
    //   {
    //     test: /\.js$/,
    //     loader: 'babel-loader',
    //     exclude: /(node_modules|bower_components)/,
    //     options: {
    //       presets: ['env'],
    //       plugins: [require('babel-plugin-transform-object-rest-spread')]
    //     }
    //   }
    // ]
  }
}